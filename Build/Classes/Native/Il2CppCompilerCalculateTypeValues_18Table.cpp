﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule>
struct List_1_t3418373063;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// UnityEngine.Analytics.TriggerListContainer
struct TriggerListContainer_t2032715483;
// UnityEngine.Analytics.EventTrigger/OnTrigger
struct OnTrigger_t4184125570;
// UnityEngine.Analytics.TriggerMethod
struct TriggerMethod_t582536534;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// HeroController
struct HeroController_t1470947597;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.Sprite[0...,0...]
struct SpriteU5B0___U2C0___U5D_t2581906350;
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t911335936;
// UnityEngine.UI.Text
struct Text_t1901882714;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef TRIGGERMETHOD_T582536534_H
#define TRIGGERMETHOD_T582536534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerMethod
struct  TriggerMethod_t582536534  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMETHOD_T582536534_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef TRACKABLETRIGGER_T621205209_H
#define TRACKABLETRIGGER_T621205209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableTrigger
struct  TrackableTrigger_t621205209  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Analytics.TrackableTrigger::m_Target
	GameObject_t1113636619 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackableTrigger::m_MethodPath
	String_t* ___m_MethodPath_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_Target_0)); }
	inline GameObject_t1113636619 * get_m_Target_0() const { return ___m_Target_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(GameObject_t1113636619 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodPath_1() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_MethodPath_1)); }
	inline String_t* get_m_MethodPath_1() const { return ___m_MethodPath_1; }
	inline String_t** get_address_of_m_MethodPath_1() { return &___m_MethodPath_1; }
	inline void set_m_MethodPath_1(String_t* value)
	{
		___m_MethodPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLETRIGGER_T621205209_H
#ifndef TRIGGERLISTCONTAINER_T2032715483_H
#define TRIGGERLISTCONTAINER_T2032715483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerListContainer
struct  TriggerListContainer_t2032715483  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule> UnityEngine.Analytics.TriggerListContainer::m_Rules
	List_1_t3418373063 * ___m_Rules_0;

public:
	inline static int32_t get_offset_of_m_Rules_0() { return static_cast<int32_t>(offsetof(TriggerListContainer_t2032715483, ___m_Rules_0)); }
	inline List_1_t3418373063 * get_m_Rules_0() const { return ___m_Rules_0; }
	inline List_1_t3418373063 ** get_address_of_m_Rules_0() { return &___m_Rules_0; }
	inline void set_m_Rules_0(List_1_t3418373063 * value)
	{
		___m_Rules_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLISTCONTAINER_T2032715483_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef SCENE_T2348375561_H
#define SCENE_T2348375561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t2348375561 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t2348375561, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T2348375561_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TRIGGERLIFECYCLEEVENT_T3193146760_H
#define TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerLifecycleEvent
struct  TriggerLifecycleEvent_t3193146760 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerLifecycleEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerLifecycleEvent_t3193146760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifndef TRIGGERBOOL_T501031542_H
#define TRIGGERBOOL_T501031542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerBool
struct  TriggerBool_t501031542 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerBool_t501031542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERBOOL_T501031542_H
#ifndef TRIGGEROPERATOR_T3611898925_H
#define TRIGGEROPERATOR_T3611898925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerOperator
struct  TriggerOperator_t3611898925 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerOperator::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerOperator_t3611898925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROPERATOR_T3611898925_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TRIGGERTYPE_T105272677_H
#define TRIGGERTYPE_T105272677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerType
struct  TriggerType_t105272677 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t105272677, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T105272677_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef TRIGGERRULE_T1946298321_H
#define TRIGGERRULE_T1946298321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerRule
struct  TriggerRule_t1946298321  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.TriggerRule::m_Target
	TrackableField_t1772682203 * ___m_Target_0;
	// UnityEngine.Analytics.TriggerOperator UnityEngine.Analytics.TriggerRule::m_Operator
	int32_t ___m_Operator_1;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value
	ValueProperty_t1868393739 * ___m_Value_2;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value2
	ValueProperty_t1868393739 * ___m_Value2_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Target_0)); }
	inline TrackableField_t1772682203 * get_m_Target_0() const { return ___m_Target_0; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(TrackableField_t1772682203 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Operator_1() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Operator_1)); }
	inline int32_t get_m_Operator_1() const { return ___m_Operator_1; }
	inline int32_t* get_address_of_m_Operator_1() { return &___m_Operator_1; }
	inline void set_m_Operator_1(int32_t value)
	{
		___m_Operator_1 = value;
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value_2)); }
	inline ValueProperty_t1868393739 * get_m_Value_2() const { return ___m_Value_2; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(ValueProperty_t1868393739 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}

	inline static int32_t get_offset_of_m_Value2_3() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value2_3)); }
	inline ValueProperty_t1868393739 * get_m_Value2_3() const { return ___m_Value2_3; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value2_3() { return &___m_Value2_3; }
	inline void set_m_Value2_3(ValueProperty_t1868393739 * value)
	{
		___m_Value2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERRULE_T1946298321_H
#ifndef EVENTTRIGGER_T2527451695_H
#define EVENTTRIGGER_T2527451695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger
struct  EventTrigger_t2527451695  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_IsTriggerExpanded
	bool ___m_IsTriggerExpanded_0;
	// UnityEngine.Analytics.TriggerType UnityEngine.Analytics.EventTrigger::m_Type
	int32_t ___m_Type_1;
	// UnityEngine.Analytics.TriggerLifecycleEvent UnityEngine.Analytics.EventTrigger::m_LifecycleEvent
	int32_t ___m_LifecycleEvent_2;
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_ApplyRules
	bool ___m_ApplyRules_3;
	// UnityEngine.Analytics.TriggerListContainer UnityEngine.Analytics.EventTrigger::m_Rules
	TriggerListContainer_t2032715483 * ___m_Rules_4;
	// UnityEngine.Analytics.TriggerBool UnityEngine.Analytics.EventTrigger::m_TriggerBool
	int32_t ___m_TriggerBool_5;
	// System.Single UnityEngine.Analytics.EventTrigger::m_InitTime
	float ___m_InitTime_6;
	// System.Single UnityEngine.Analytics.EventTrigger::m_RepeatTime
	float ___m_RepeatTime_7;
	// System.Int32 UnityEngine.Analytics.EventTrigger::m_Repetitions
	int32_t ___m_Repetitions_8;
	// System.Int32 UnityEngine.Analytics.EventTrigger::repetitionCount
	int32_t ___repetitionCount_9;
	// UnityEngine.Analytics.EventTrigger/OnTrigger UnityEngine.Analytics.EventTrigger::m_TriggerFunction
	OnTrigger_t4184125570 * ___m_TriggerFunction_10;
	// UnityEngine.Analytics.TriggerMethod UnityEngine.Analytics.EventTrigger::m_Method
	TriggerMethod_t582536534 * ___m_Method_11;

public:
	inline static int32_t get_offset_of_m_IsTriggerExpanded_0() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_IsTriggerExpanded_0)); }
	inline bool get_m_IsTriggerExpanded_0() const { return ___m_IsTriggerExpanded_0; }
	inline bool* get_address_of_m_IsTriggerExpanded_0() { return &___m_IsTriggerExpanded_0; }
	inline void set_m_IsTriggerExpanded_0(bool value)
	{
		___m_IsTriggerExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_LifecycleEvent_2() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_LifecycleEvent_2)); }
	inline int32_t get_m_LifecycleEvent_2() const { return ___m_LifecycleEvent_2; }
	inline int32_t* get_address_of_m_LifecycleEvent_2() { return &___m_LifecycleEvent_2; }
	inline void set_m_LifecycleEvent_2(int32_t value)
	{
		___m_LifecycleEvent_2 = value;
	}

	inline static int32_t get_offset_of_m_ApplyRules_3() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_ApplyRules_3)); }
	inline bool get_m_ApplyRules_3() const { return ___m_ApplyRules_3; }
	inline bool* get_address_of_m_ApplyRules_3() { return &___m_ApplyRules_3; }
	inline void set_m_ApplyRules_3(bool value)
	{
		___m_ApplyRules_3 = value;
	}

	inline static int32_t get_offset_of_m_Rules_4() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Rules_4)); }
	inline TriggerListContainer_t2032715483 * get_m_Rules_4() const { return ___m_Rules_4; }
	inline TriggerListContainer_t2032715483 ** get_address_of_m_Rules_4() { return &___m_Rules_4; }
	inline void set_m_Rules_4(TriggerListContainer_t2032715483 * value)
	{
		___m_Rules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_4), value);
	}

	inline static int32_t get_offset_of_m_TriggerBool_5() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerBool_5)); }
	inline int32_t get_m_TriggerBool_5() const { return ___m_TriggerBool_5; }
	inline int32_t* get_address_of_m_TriggerBool_5() { return &___m_TriggerBool_5; }
	inline void set_m_TriggerBool_5(int32_t value)
	{
		___m_TriggerBool_5 = value;
	}

	inline static int32_t get_offset_of_m_InitTime_6() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_InitTime_6)); }
	inline float get_m_InitTime_6() const { return ___m_InitTime_6; }
	inline float* get_address_of_m_InitTime_6() { return &___m_InitTime_6; }
	inline void set_m_InitTime_6(float value)
	{
		___m_InitTime_6 = value;
	}

	inline static int32_t get_offset_of_m_RepeatTime_7() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_RepeatTime_7)); }
	inline float get_m_RepeatTime_7() const { return ___m_RepeatTime_7; }
	inline float* get_address_of_m_RepeatTime_7() { return &___m_RepeatTime_7; }
	inline void set_m_RepeatTime_7(float value)
	{
		___m_RepeatTime_7 = value;
	}

	inline static int32_t get_offset_of_m_Repetitions_8() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Repetitions_8)); }
	inline int32_t get_m_Repetitions_8() const { return ___m_Repetitions_8; }
	inline int32_t* get_address_of_m_Repetitions_8() { return &___m_Repetitions_8; }
	inline void set_m_Repetitions_8(int32_t value)
	{
		___m_Repetitions_8 = value;
	}

	inline static int32_t get_offset_of_repetitionCount_9() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___repetitionCount_9)); }
	inline int32_t get_repetitionCount_9() const { return ___repetitionCount_9; }
	inline int32_t* get_address_of_repetitionCount_9() { return &___repetitionCount_9; }
	inline void set_repetitionCount_9(int32_t value)
	{
		___repetitionCount_9 = value;
	}

	inline static int32_t get_offset_of_m_TriggerFunction_10() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerFunction_10)); }
	inline OnTrigger_t4184125570 * get_m_TriggerFunction_10() const { return ___m_TriggerFunction_10; }
	inline OnTrigger_t4184125570 ** get_address_of_m_TriggerFunction_10() { return &___m_TriggerFunction_10; }
	inline void set_m_TriggerFunction_10(OnTrigger_t4184125570 * value)
	{
		___m_TriggerFunction_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerFunction_10), value);
	}

	inline static int32_t get_offset_of_m_Method_11() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Method_11)); }
	inline TriggerMethod_t582536534 * get_m_Method_11() const { return ___m_Method_11; }
	inline TriggerMethod_t582536534 ** get_address_of_m_Method_11() { return &___m_Method_11; }
	inline void set_m_Method_11(TriggerMethod_t582536534 * value)
	{
		___m_Method_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T2527451695_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef ONTRIGGER_T4184125570_H
#define ONTRIGGER_T4184125570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger/OnTrigger
struct  OnTrigger_t4184125570  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGER_T4184125570_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CAMERA_T4174856228_H
#define CAMERA_T4174856228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Camera
struct  Camera_t4174856228  : public MonoBehaviour_t3962482529
{
public:
	// HeroController Camera::hero
	HeroController_t1470947597 * ___hero_2;
	// UnityEngine.Vector3 Camera::pos
	Vector3_t3722313464  ___pos_3;
	// UnityEngine.Vector3 Camera::coord
	Vector3_t3722313464  ___coord_4;
	// System.Boolean Camera::game
	bool ___game_5;

public:
	inline static int32_t get_offset_of_hero_2() { return static_cast<int32_t>(offsetof(Camera_t4174856228, ___hero_2)); }
	inline HeroController_t1470947597 * get_hero_2() const { return ___hero_2; }
	inline HeroController_t1470947597 ** get_address_of_hero_2() { return &___hero_2; }
	inline void set_hero_2(HeroController_t1470947597 * value)
	{
		___hero_2 = value;
		Il2CppCodeGenWriteBarrier((&___hero_2), value);
	}

	inline static int32_t get_offset_of_pos_3() { return static_cast<int32_t>(offsetof(Camera_t4174856228, ___pos_3)); }
	inline Vector3_t3722313464  get_pos_3() const { return ___pos_3; }
	inline Vector3_t3722313464 * get_address_of_pos_3() { return &___pos_3; }
	inline void set_pos_3(Vector3_t3722313464  value)
	{
		___pos_3 = value;
	}

	inline static int32_t get_offset_of_coord_4() { return static_cast<int32_t>(offsetof(Camera_t4174856228, ___coord_4)); }
	inline Vector3_t3722313464  get_coord_4() const { return ___coord_4; }
	inline Vector3_t3722313464 * get_address_of_coord_4() { return &___coord_4; }
	inline void set_coord_4(Vector3_t3722313464  value)
	{
		___coord_4 = value;
	}

	inline static int32_t get_offset_of_game_5() { return static_cast<int32_t>(offsetof(Camera_t4174856228, ___game_5)); }
	inline bool get_game_5() const { return ___game_5; }
	inline bool* get_address_of_game_5() { return &___game_5; }
	inline void set_game_5(bool value)
	{
		___game_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T4174856228_H
#ifndef LEVELCONTROLLER_T24418946_H
#define LEVELCONTROLLER_T24418946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelController
struct  LevelController_t24418946  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject LevelController::platformPrefab
	GameObject_t1113636619 * ___platformPrefab_2;
	// UnityEngine.Vector3 LevelController::startPosition
	Vector3_t3722313464  ___startPosition_3;
	// System.Int32 LevelController::countPlatform
	int32_t ___countPlatform_4;
	// System.Int32 LevelController::changePos
	int32_t ___changePos_5;
	// UnityEngine.GameObject[] LevelController::platforms
	GameObjectU5BU5D_t3328599146* ___platforms_6;
	// System.Int32 LevelController::localScore
	int32_t ___localScore_7;

public:
	inline static int32_t get_offset_of_platformPrefab_2() { return static_cast<int32_t>(offsetof(LevelController_t24418946, ___platformPrefab_2)); }
	inline GameObject_t1113636619 * get_platformPrefab_2() const { return ___platformPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_platformPrefab_2() { return &___platformPrefab_2; }
	inline void set_platformPrefab_2(GameObject_t1113636619 * value)
	{
		___platformPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___platformPrefab_2), value);
	}

	inline static int32_t get_offset_of_startPosition_3() { return static_cast<int32_t>(offsetof(LevelController_t24418946, ___startPosition_3)); }
	inline Vector3_t3722313464  get_startPosition_3() const { return ___startPosition_3; }
	inline Vector3_t3722313464 * get_address_of_startPosition_3() { return &___startPosition_3; }
	inline void set_startPosition_3(Vector3_t3722313464  value)
	{
		___startPosition_3 = value;
	}

	inline static int32_t get_offset_of_countPlatform_4() { return static_cast<int32_t>(offsetof(LevelController_t24418946, ___countPlatform_4)); }
	inline int32_t get_countPlatform_4() const { return ___countPlatform_4; }
	inline int32_t* get_address_of_countPlatform_4() { return &___countPlatform_4; }
	inline void set_countPlatform_4(int32_t value)
	{
		___countPlatform_4 = value;
	}

	inline static int32_t get_offset_of_changePos_5() { return static_cast<int32_t>(offsetof(LevelController_t24418946, ___changePos_5)); }
	inline int32_t get_changePos_5() const { return ___changePos_5; }
	inline int32_t* get_address_of_changePos_5() { return &___changePos_5; }
	inline void set_changePos_5(int32_t value)
	{
		___changePos_5 = value;
	}

	inline static int32_t get_offset_of_platforms_6() { return static_cast<int32_t>(offsetof(LevelController_t24418946, ___platforms_6)); }
	inline GameObjectU5BU5D_t3328599146* get_platforms_6() const { return ___platforms_6; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_platforms_6() { return &___platforms_6; }
	inline void set_platforms_6(GameObjectU5BU5D_t3328599146* value)
	{
		___platforms_6 = value;
		Il2CppCodeGenWriteBarrier((&___platforms_6), value);
	}

	inline static int32_t get_offset_of_localScore_7() { return static_cast<int32_t>(offsetof(LevelController_t24418946, ___localScore_7)); }
	inline int32_t get_localScore_7() const { return ___localScore_7; }
	inline int32_t* get_address_of_localScore_7() { return &___localScore_7; }
	inline void set_localScore_7(int32_t value)
	{
		___localScore_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCONTROLLER_T24418946_H
#ifndef PLATFORMCONTROLLER_T389759191_H
#define PLATFORMCONTROLLER_T389759191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlatformController
struct  PlatformController_t389759191  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] PlatformController::gates
	GameObjectU5BU5D_t3328599146* ___gates_2;
	// UnityEngine.GameObject[] PlatformController::instGates
	GameObjectU5BU5D_t3328599146* ___instGates_3;
	// UnityEngine.GameObject PlatformController::groundPrefab
	GameObject_t1113636619 * ___groundPrefab_4;
	// UnityEngine.GameObject PlatformController::ground
	GameObject_t1113636619 * ___ground_5;
	// UnityEngine.Vector3[] PlatformController::positions
	Vector3U5BU5D_t1718750761* ___positions_6;
	// UnityEngine.Vector3 PlatformController::positionGround
	Vector3_t3722313464  ___positionGround_7;

public:
	inline static int32_t get_offset_of_gates_2() { return static_cast<int32_t>(offsetof(PlatformController_t389759191, ___gates_2)); }
	inline GameObjectU5BU5D_t3328599146* get_gates_2() const { return ___gates_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_gates_2() { return &___gates_2; }
	inline void set_gates_2(GameObjectU5BU5D_t3328599146* value)
	{
		___gates_2 = value;
		Il2CppCodeGenWriteBarrier((&___gates_2), value);
	}

	inline static int32_t get_offset_of_instGates_3() { return static_cast<int32_t>(offsetof(PlatformController_t389759191, ___instGates_3)); }
	inline GameObjectU5BU5D_t3328599146* get_instGates_3() const { return ___instGates_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_instGates_3() { return &___instGates_3; }
	inline void set_instGates_3(GameObjectU5BU5D_t3328599146* value)
	{
		___instGates_3 = value;
		Il2CppCodeGenWriteBarrier((&___instGates_3), value);
	}

	inline static int32_t get_offset_of_groundPrefab_4() { return static_cast<int32_t>(offsetof(PlatformController_t389759191, ___groundPrefab_4)); }
	inline GameObject_t1113636619 * get_groundPrefab_4() const { return ___groundPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_groundPrefab_4() { return &___groundPrefab_4; }
	inline void set_groundPrefab_4(GameObject_t1113636619 * value)
	{
		___groundPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___groundPrefab_4), value);
	}

	inline static int32_t get_offset_of_ground_5() { return static_cast<int32_t>(offsetof(PlatformController_t389759191, ___ground_5)); }
	inline GameObject_t1113636619 * get_ground_5() const { return ___ground_5; }
	inline GameObject_t1113636619 ** get_address_of_ground_5() { return &___ground_5; }
	inline void set_ground_5(GameObject_t1113636619 * value)
	{
		___ground_5 = value;
		Il2CppCodeGenWriteBarrier((&___ground_5), value);
	}

	inline static int32_t get_offset_of_positions_6() { return static_cast<int32_t>(offsetof(PlatformController_t389759191, ___positions_6)); }
	inline Vector3U5BU5D_t1718750761* get_positions_6() const { return ___positions_6; }
	inline Vector3U5BU5D_t1718750761** get_address_of_positions_6() { return &___positions_6; }
	inline void set_positions_6(Vector3U5BU5D_t1718750761* value)
	{
		___positions_6 = value;
		Il2CppCodeGenWriteBarrier((&___positions_6), value);
	}

	inline static int32_t get_offset_of_positionGround_7() { return static_cast<int32_t>(offsetof(PlatformController_t389759191, ___positionGround_7)); }
	inline Vector3_t3722313464  get_positionGround_7() const { return ___positionGround_7; }
	inline Vector3_t3722313464 * get_address_of_positionGround_7() { return &___positionGround_7; }
	inline void set_positionGround_7(Vector3_t3722313464  value)
	{
		___positionGround_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMCONTROLLER_T389759191_H
#ifndef JUMPZONE_T2032044937_H
#define JUMPZONE_T2032044937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JumpZone
struct  JumpZone_t2032044937  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject JumpZone::player
	GameObject_t1113636619 * ___player_2;
	// HeroController JumpZone::hero
	HeroController_t1470947597 * ___hero_3;

public:
	inline static int32_t get_offset_of_player_2() { return static_cast<int32_t>(offsetof(JumpZone_t2032044937, ___player_2)); }
	inline GameObject_t1113636619 * get_player_2() const { return ___player_2; }
	inline GameObject_t1113636619 ** get_address_of_player_2() { return &___player_2; }
	inline void set_player_2(GameObject_t1113636619 * value)
	{
		___player_2 = value;
		Il2CppCodeGenWriteBarrier((&___player_2), value);
	}

	inline static int32_t get_offset_of_hero_3() { return static_cast<int32_t>(offsetof(JumpZone_t2032044937, ___hero_3)); }
	inline HeroController_t1470947597 * get_hero_3() const { return ___hero_3; }
	inline HeroController_t1470947597 ** get_address_of_hero_3() { return &___hero_3; }
	inline void set_hero_3(HeroController_t1470947597 * value)
	{
		___hero_3 = value;
		Il2CppCodeGenWriteBarrier((&___hero_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JUMPZONE_T2032044937_H
#ifndef HEROCONTROLLER_T1470947597_H
#define HEROCONTROLLER_T1470947597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroController
struct  HeroController_t1470947597  : public MonoBehaviour_t3962482529
{
public:
	// System.Single HeroController::speed
	float ___speed_2;
	// System.Single HeroController::jumpForce
	float ___jumpForce_3;
	// System.Single HeroController::maxSpeed
	float ___maxSpeed_4;
	// System.Single HeroController::increaseSpeed
	float ___increaseSpeed_5;
	// UnityEngine.Sprite HeroController::yelCirc
	Sprite_t280657092 * ___yelCirc_6;
	// UnityEngine.Sprite HeroController::yelClock
	Sprite_t280657092 * ___yelClock_7;
	// UnityEngine.Sprite HeroController::yelRhomb
	Sprite_t280657092 * ___yelRhomb_8;
	// UnityEngine.Sprite HeroController::redCirc
	Sprite_t280657092 * ___redCirc_9;
	// UnityEngine.Sprite HeroController::redClock
	Sprite_t280657092 * ___redClock_10;
	// UnityEngine.Sprite HeroController::redRhomb
	Sprite_t280657092 * ___redRhomb_11;
	// UnityEngine.Sprite HeroController::violetCirc
	Sprite_t280657092 * ___violetCirc_12;
	// UnityEngine.Sprite HeroController::violetClock
	Sprite_t280657092 * ___violetClock_13;
	// UnityEngine.Sprite HeroController::violetRhomb
	Sprite_t280657092 * ___violetRhomb_14;
	// System.Boolean HeroController::isMove
	bool ___isMove_15;
	// System.Boolean HeroController::cameraStart
	bool ___cameraStart_16;
	// System.Boolean HeroController::isDeath
	bool ___isDeath_17;
	// System.Boolean HeroController::isGrounded
	bool ___isGrounded_18;
	// System.Boolean HeroController::isJump
	bool ___isJump_19;
	// UnityEngine.SpriteRenderer HeroController::heroSprite
	SpriteRenderer_t3235626157 * ___heroSprite_20;
	// UnityEngine.Rigidbody2D HeroController::rb
	Rigidbody2D_t939494601 * ___rb_21;
	// UnityEngine.Sprite[0...,0...] HeroController::sprites
	SpriteU5B0___U2C0___U5D_t2581906350* ___sprites_22;
	// System.Int32 HeroController::color
	int32_t ___color_23;
	// System.Int32 HeroController::shape
	int32_t ___shape_24;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_jumpForce_3() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___jumpForce_3)); }
	inline float get_jumpForce_3() const { return ___jumpForce_3; }
	inline float* get_address_of_jumpForce_3() { return &___jumpForce_3; }
	inline void set_jumpForce_3(float value)
	{
		___jumpForce_3 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_4() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___maxSpeed_4)); }
	inline float get_maxSpeed_4() const { return ___maxSpeed_4; }
	inline float* get_address_of_maxSpeed_4() { return &___maxSpeed_4; }
	inline void set_maxSpeed_4(float value)
	{
		___maxSpeed_4 = value;
	}

	inline static int32_t get_offset_of_increaseSpeed_5() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___increaseSpeed_5)); }
	inline float get_increaseSpeed_5() const { return ___increaseSpeed_5; }
	inline float* get_address_of_increaseSpeed_5() { return &___increaseSpeed_5; }
	inline void set_increaseSpeed_5(float value)
	{
		___increaseSpeed_5 = value;
	}

	inline static int32_t get_offset_of_yelCirc_6() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___yelCirc_6)); }
	inline Sprite_t280657092 * get_yelCirc_6() const { return ___yelCirc_6; }
	inline Sprite_t280657092 ** get_address_of_yelCirc_6() { return &___yelCirc_6; }
	inline void set_yelCirc_6(Sprite_t280657092 * value)
	{
		___yelCirc_6 = value;
		Il2CppCodeGenWriteBarrier((&___yelCirc_6), value);
	}

	inline static int32_t get_offset_of_yelClock_7() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___yelClock_7)); }
	inline Sprite_t280657092 * get_yelClock_7() const { return ___yelClock_7; }
	inline Sprite_t280657092 ** get_address_of_yelClock_7() { return &___yelClock_7; }
	inline void set_yelClock_7(Sprite_t280657092 * value)
	{
		___yelClock_7 = value;
		Il2CppCodeGenWriteBarrier((&___yelClock_7), value);
	}

	inline static int32_t get_offset_of_yelRhomb_8() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___yelRhomb_8)); }
	inline Sprite_t280657092 * get_yelRhomb_8() const { return ___yelRhomb_8; }
	inline Sprite_t280657092 ** get_address_of_yelRhomb_8() { return &___yelRhomb_8; }
	inline void set_yelRhomb_8(Sprite_t280657092 * value)
	{
		___yelRhomb_8 = value;
		Il2CppCodeGenWriteBarrier((&___yelRhomb_8), value);
	}

	inline static int32_t get_offset_of_redCirc_9() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___redCirc_9)); }
	inline Sprite_t280657092 * get_redCirc_9() const { return ___redCirc_9; }
	inline Sprite_t280657092 ** get_address_of_redCirc_9() { return &___redCirc_9; }
	inline void set_redCirc_9(Sprite_t280657092 * value)
	{
		___redCirc_9 = value;
		Il2CppCodeGenWriteBarrier((&___redCirc_9), value);
	}

	inline static int32_t get_offset_of_redClock_10() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___redClock_10)); }
	inline Sprite_t280657092 * get_redClock_10() const { return ___redClock_10; }
	inline Sprite_t280657092 ** get_address_of_redClock_10() { return &___redClock_10; }
	inline void set_redClock_10(Sprite_t280657092 * value)
	{
		___redClock_10 = value;
		Il2CppCodeGenWriteBarrier((&___redClock_10), value);
	}

	inline static int32_t get_offset_of_redRhomb_11() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___redRhomb_11)); }
	inline Sprite_t280657092 * get_redRhomb_11() const { return ___redRhomb_11; }
	inline Sprite_t280657092 ** get_address_of_redRhomb_11() { return &___redRhomb_11; }
	inline void set_redRhomb_11(Sprite_t280657092 * value)
	{
		___redRhomb_11 = value;
		Il2CppCodeGenWriteBarrier((&___redRhomb_11), value);
	}

	inline static int32_t get_offset_of_violetCirc_12() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___violetCirc_12)); }
	inline Sprite_t280657092 * get_violetCirc_12() const { return ___violetCirc_12; }
	inline Sprite_t280657092 ** get_address_of_violetCirc_12() { return &___violetCirc_12; }
	inline void set_violetCirc_12(Sprite_t280657092 * value)
	{
		___violetCirc_12 = value;
		Il2CppCodeGenWriteBarrier((&___violetCirc_12), value);
	}

	inline static int32_t get_offset_of_violetClock_13() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___violetClock_13)); }
	inline Sprite_t280657092 * get_violetClock_13() const { return ___violetClock_13; }
	inline Sprite_t280657092 ** get_address_of_violetClock_13() { return &___violetClock_13; }
	inline void set_violetClock_13(Sprite_t280657092 * value)
	{
		___violetClock_13 = value;
		Il2CppCodeGenWriteBarrier((&___violetClock_13), value);
	}

	inline static int32_t get_offset_of_violetRhomb_14() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___violetRhomb_14)); }
	inline Sprite_t280657092 * get_violetRhomb_14() const { return ___violetRhomb_14; }
	inline Sprite_t280657092 ** get_address_of_violetRhomb_14() { return &___violetRhomb_14; }
	inline void set_violetRhomb_14(Sprite_t280657092 * value)
	{
		___violetRhomb_14 = value;
		Il2CppCodeGenWriteBarrier((&___violetRhomb_14), value);
	}

	inline static int32_t get_offset_of_isMove_15() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___isMove_15)); }
	inline bool get_isMove_15() const { return ___isMove_15; }
	inline bool* get_address_of_isMove_15() { return &___isMove_15; }
	inline void set_isMove_15(bool value)
	{
		___isMove_15 = value;
	}

	inline static int32_t get_offset_of_cameraStart_16() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___cameraStart_16)); }
	inline bool get_cameraStart_16() const { return ___cameraStart_16; }
	inline bool* get_address_of_cameraStart_16() { return &___cameraStart_16; }
	inline void set_cameraStart_16(bool value)
	{
		___cameraStart_16 = value;
	}

	inline static int32_t get_offset_of_isDeath_17() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___isDeath_17)); }
	inline bool get_isDeath_17() const { return ___isDeath_17; }
	inline bool* get_address_of_isDeath_17() { return &___isDeath_17; }
	inline void set_isDeath_17(bool value)
	{
		___isDeath_17 = value;
	}

	inline static int32_t get_offset_of_isGrounded_18() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___isGrounded_18)); }
	inline bool get_isGrounded_18() const { return ___isGrounded_18; }
	inline bool* get_address_of_isGrounded_18() { return &___isGrounded_18; }
	inline void set_isGrounded_18(bool value)
	{
		___isGrounded_18 = value;
	}

	inline static int32_t get_offset_of_isJump_19() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___isJump_19)); }
	inline bool get_isJump_19() const { return ___isJump_19; }
	inline bool* get_address_of_isJump_19() { return &___isJump_19; }
	inline void set_isJump_19(bool value)
	{
		___isJump_19 = value;
	}

	inline static int32_t get_offset_of_heroSprite_20() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___heroSprite_20)); }
	inline SpriteRenderer_t3235626157 * get_heroSprite_20() const { return ___heroSprite_20; }
	inline SpriteRenderer_t3235626157 ** get_address_of_heroSprite_20() { return &___heroSprite_20; }
	inline void set_heroSprite_20(SpriteRenderer_t3235626157 * value)
	{
		___heroSprite_20 = value;
		Il2CppCodeGenWriteBarrier((&___heroSprite_20), value);
	}

	inline static int32_t get_offset_of_rb_21() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___rb_21)); }
	inline Rigidbody2D_t939494601 * get_rb_21() const { return ___rb_21; }
	inline Rigidbody2D_t939494601 ** get_address_of_rb_21() { return &___rb_21; }
	inline void set_rb_21(Rigidbody2D_t939494601 * value)
	{
		___rb_21 = value;
		Il2CppCodeGenWriteBarrier((&___rb_21), value);
	}

	inline static int32_t get_offset_of_sprites_22() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___sprites_22)); }
	inline SpriteU5B0___U2C0___U5D_t2581906350* get_sprites_22() const { return ___sprites_22; }
	inline SpriteU5B0___U2C0___U5D_t2581906350** get_address_of_sprites_22() { return &___sprites_22; }
	inline void set_sprites_22(SpriteU5B0___U2C0___U5D_t2581906350* value)
	{
		___sprites_22 = value;
		Il2CppCodeGenWriteBarrier((&___sprites_22), value);
	}

	inline static int32_t get_offset_of_color_23() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___color_23)); }
	inline int32_t get_color_23() const { return ___color_23; }
	inline int32_t* get_address_of_color_23() { return &___color_23; }
	inline void set_color_23(int32_t value)
	{
		___color_23 = value;
	}

	inline static int32_t get_offset_of_shape_24() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___shape_24)); }
	inline int32_t get_shape_24() const { return ___shape_24; }
	inline int32_t* get_address_of_shape_24() { return &___shape_24; }
	inline void set_shape_24(int32_t value)
	{
		___shape_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROCONTROLLER_T1470947597_H
#ifndef GATECONTROLLER_T2541762910_H
#define GATECONTROLLER_T2541762910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GateController
struct  GateController_t2541762910  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject GateController::player
	GameObject_t1113636619 * ___player_2;
	// HeroController GateController::hero
	HeroController_t1470947597 * ___hero_3;
	// UnityEngine.SpriteRenderer[] GateController::sprites
	SpriteRendererU5BU5D_t911335936* ___sprites_4;
	// System.Int32 GateController::colorGate
	int32_t ___colorGate_5;
	// System.Int32 GateController::shapeGate
	int32_t ___shapeGate_6;

public:
	inline static int32_t get_offset_of_player_2() { return static_cast<int32_t>(offsetof(GateController_t2541762910, ___player_2)); }
	inline GameObject_t1113636619 * get_player_2() const { return ___player_2; }
	inline GameObject_t1113636619 ** get_address_of_player_2() { return &___player_2; }
	inline void set_player_2(GameObject_t1113636619 * value)
	{
		___player_2 = value;
		Il2CppCodeGenWriteBarrier((&___player_2), value);
	}

	inline static int32_t get_offset_of_hero_3() { return static_cast<int32_t>(offsetof(GateController_t2541762910, ___hero_3)); }
	inline HeroController_t1470947597 * get_hero_3() const { return ___hero_3; }
	inline HeroController_t1470947597 ** get_address_of_hero_3() { return &___hero_3; }
	inline void set_hero_3(HeroController_t1470947597 * value)
	{
		___hero_3 = value;
		Il2CppCodeGenWriteBarrier((&___hero_3), value);
	}

	inline static int32_t get_offset_of_sprites_4() { return static_cast<int32_t>(offsetof(GateController_t2541762910, ___sprites_4)); }
	inline SpriteRendererU5BU5D_t911335936* get_sprites_4() const { return ___sprites_4; }
	inline SpriteRendererU5BU5D_t911335936** get_address_of_sprites_4() { return &___sprites_4; }
	inline void set_sprites_4(SpriteRendererU5BU5D_t911335936* value)
	{
		___sprites_4 = value;
		Il2CppCodeGenWriteBarrier((&___sprites_4), value);
	}

	inline static int32_t get_offset_of_colorGate_5() { return static_cast<int32_t>(offsetof(GateController_t2541762910, ___colorGate_5)); }
	inline int32_t get_colorGate_5() const { return ___colorGate_5; }
	inline int32_t* get_address_of_colorGate_5() { return &___colorGate_5; }
	inline void set_colorGate_5(int32_t value)
	{
		___colorGate_5 = value;
	}

	inline static int32_t get_offset_of_shapeGate_6() { return static_cast<int32_t>(offsetof(GateController_t2541762910, ___shapeGate_6)); }
	inline int32_t get_shapeGate_6() const { return ___shapeGate_6; }
	inline int32_t* get_address_of_shapeGate_6() { return &___shapeGate_6; }
	inline void set_shapeGate_6(int32_t value)
	{
		___shapeGate_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GATECONTROLLER_T2541762910_H
#ifndef GAMECONTROLLER_T2330501625_H
#define GAMECONTROLLER_T2330501625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController
struct  GameController_t2330501625  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text GameController::scoreText
	Text_t1901882714 * ___scoreText_2;
	// UnityEngine.UI.Text GameController::deathScore
	Text_t1901882714 * ___deathScore_3;
	// UnityEngine.UI.Text GameController::record
	Text_t1901882714 * ___record_4;
	// HeroController GameController::hero
	HeroController_t1470947597 * ___hero_5;
	// UnityEngine.GameObject GameController::GameGUI
	GameObject_t1113636619 * ___GameGUI_6;
	// UnityEngine.GameObject GameController::StartGUI
	GameObject_t1113636619 * ___StartGUI_7;
	// UnityEngine.GameObject GameController::SettingsGUI
	GameObject_t1113636619 * ___SettingsGUI_8;
	// UnityEngine.GameObject GameController::DeathGUI
	GameObject_t1113636619 * ___DeathGUI_9;
	// UnityEngine.GameObject GameController::mainCamera
	GameObject_t1113636619 * ___mainCamera_10;
	// UnityEngine.SceneManagement.Scene GameController::scene
	Scene_t2348375561  ___scene_11;

public:
	inline static int32_t get_offset_of_scoreText_2() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___scoreText_2)); }
	inline Text_t1901882714 * get_scoreText_2() const { return ___scoreText_2; }
	inline Text_t1901882714 ** get_address_of_scoreText_2() { return &___scoreText_2; }
	inline void set_scoreText_2(Text_t1901882714 * value)
	{
		___scoreText_2 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_2), value);
	}

	inline static int32_t get_offset_of_deathScore_3() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___deathScore_3)); }
	inline Text_t1901882714 * get_deathScore_3() const { return ___deathScore_3; }
	inline Text_t1901882714 ** get_address_of_deathScore_3() { return &___deathScore_3; }
	inline void set_deathScore_3(Text_t1901882714 * value)
	{
		___deathScore_3 = value;
		Il2CppCodeGenWriteBarrier((&___deathScore_3), value);
	}

	inline static int32_t get_offset_of_record_4() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___record_4)); }
	inline Text_t1901882714 * get_record_4() const { return ___record_4; }
	inline Text_t1901882714 ** get_address_of_record_4() { return &___record_4; }
	inline void set_record_4(Text_t1901882714 * value)
	{
		___record_4 = value;
		Il2CppCodeGenWriteBarrier((&___record_4), value);
	}

	inline static int32_t get_offset_of_hero_5() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___hero_5)); }
	inline HeroController_t1470947597 * get_hero_5() const { return ___hero_5; }
	inline HeroController_t1470947597 ** get_address_of_hero_5() { return &___hero_5; }
	inline void set_hero_5(HeroController_t1470947597 * value)
	{
		___hero_5 = value;
		Il2CppCodeGenWriteBarrier((&___hero_5), value);
	}

	inline static int32_t get_offset_of_GameGUI_6() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___GameGUI_6)); }
	inline GameObject_t1113636619 * get_GameGUI_6() const { return ___GameGUI_6; }
	inline GameObject_t1113636619 ** get_address_of_GameGUI_6() { return &___GameGUI_6; }
	inline void set_GameGUI_6(GameObject_t1113636619 * value)
	{
		___GameGUI_6 = value;
		Il2CppCodeGenWriteBarrier((&___GameGUI_6), value);
	}

	inline static int32_t get_offset_of_StartGUI_7() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___StartGUI_7)); }
	inline GameObject_t1113636619 * get_StartGUI_7() const { return ___StartGUI_7; }
	inline GameObject_t1113636619 ** get_address_of_StartGUI_7() { return &___StartGUI_7; }
	inline void set_StartGUI_7(GameObject_t1113636619 * value)
	{
		___StartGUI_7 = value;
		Il2CppCodeGenWriteBarrier((&___StartGUI_7), value);
	}

	inline static int32_t get_offset_of_SettingsGUI_8() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___SettingsGUI_8)); }
	inline GameObject_t1113636619 * get_SettingsGUI_8() const { return ___SettingsGUI_8; }
	inline GameObject_t1113636619 ** get_address_of_SettingsGUI_8() { return &___SettingsGUI_8; }
	inline void set_SettingsGUI_8(GameObject_t1113636619 * value)
	{
		___SettingsGUI_8 = value;
		Il2CppCodeGenWriteBarrier((&___SettingsGUI_8), value);
	}

	inline static int32_t get_offset_of_DeathGUI_9() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___DeathGUI_9)); }
	inline GameObject_t1113636619 * get_DeathGUI_9() const { return ___DeathGUI_9; }
	inline GameObject_t1113636619 ** get_address_of_DeathGUI_9() { return &___DeathGUI_9; }
	inline void set_DeathGUI_9(GameObject_t1113636619 * value)
	{
		___DeathGUI_9 = value;
		Il2CppCodeGenWriteBarrier((&___DeathGUI_9), value);
	}

	inline static int32_t get_offset_of_mainCamera_10() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___mainCamera_10)); }
	inline GameObject_t1113636619 * get_mainCamera_10() const { return ___mainCamera_10; }
	inline GameObject_t1113636619 ** get_address_of_mainCamera_10() { return &___mainCamera_10; }
	inline void set_mainCamera_10(GameObject_t1113636619 * value)
	{
		___mainCamera_10 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_10), value);
	}

	inline static int32_t get_offset_of_scene_11() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___scene_11)); }
	inline Scene_t2348375561  get_scene_11() const { return ___scene_11; }
	inline Scene_t2348375561 * get_address_of_scene_11() { return &___scene_11; }
	inline void set_scene_11(Scene_t2348375561  value)
	{
		___scene_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECONTROLLER_T2330501625_H
#ifndef CHECKERCONTROLLER_T1724959013_H
#define CHECKERCONTROLLER_T1724959013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CheckerController
struct  CheckerController_t1724959013  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKERCONTROLLER_T1724959013_H
#ifndef PLATFORMCOLLIDERCONTROLLER_T1042155798_H
#define PLATFORMCOLLIDERCONTROLLER_T1042155798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlatformColliderController
struct  PlatformColliderController_t1042155798  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PlatformColliderController::player
	GameObject_t1113636619 * ___player_2;
	// HeroController PlatformColliderController::hero
	HeroController_t1470947597 * ___hero_3;

public:
	inline static int32_t get_offset_of_player_2() { return static_cast<int32_t>(offsetof(PlatformColliderController_t1042155798, ___player_2)); }
	inline GameObject_t1113636619 * get_player_2() const { return ___player_2; }
	inline GameObject_t1113636619 ** get_address_of_player_2() { return &___player_2; }
	inline void set_player_2(GameObject_t1113636619 * value)
	{
		___player_2 = value;
		Il2CppCodeGenWriteBarrier((&___player_2), value);
	}

	inline static int32_t get_offset_of_hero_3() { return static_cast<int32_t>(offsetof(PlatformColliderController_t1042155798, ___hero_3)); }
	inline HeroController_t1470947597 * get_hero_3() const { return ___hero_3; }
	inline HeroController_t1470947597 ** get_address_of_hero_3() { return &___hero_3; }
	inline void set_hero_3(HeroController_t1470947597 * value)
	{
		___hero_3 = value;
		Il2CppCodeGenWriteBarrier((&___hero_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMCOLLIDERCONTROLLER_T1042155798_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (TriggerOperator_t3611898925)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1800[9] = 
{
	TriggerOperator_t3611898925::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (TriggerType_t105272677)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1801[5] = 
{
	TriggerType_t105272677::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (TriggerListContainer_t2032715483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[1] = 
{
	TriggerListContainer_t2032715483::get_offset_of_m_Rules_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (EventTrigger_t2527451695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[12] = 
{
	EventTrigger_t2527451695::get_offset_of_m_IsTriggerExpanded_0(),
	EventTrigger_t2527451695::get_offset_of_m_Type_1(),
	EventTrigger_t2527451695::get_offset_of_m_LifecycleEvent_2(),
	EventTrigger_t2527451695::get_offset_of_m_ApplyRules_3(),
	EventTrigger_t2527451695::get_offset_of_m_Rules_4(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerBool_5(),
	EventTrigger_t2527451695::get_offset_of_m_InitTime_6(),
	EventTrigger_t2527451695::get_offset_of_m_RepeatTime_7(),
	EventTrigger_t2527451695::get_offset_of_m_Repetitions_8(),
	EventTrigger_t2527451695::get_offset_of_repetitionCount_9(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerFunction_10(),
	EventTrigger_t2527451695::get_offset_of_m_Method_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (OnTrigger_t4184125570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (TrackableTrigger_t621205209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[2] = 
{
	TrackableTrigger_t621205209::get_offset_of_m_Target_0(),
	TrackableTrigger_t621205209::get_offset_of_m_MethodPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (TriggerMethod_t582536534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (TriggerRule_t1946298321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[4] = 
{
	TriggerRule_t1946298321::get_offset_of_m_Target_0(),
	TriggerRule_t1946298321::get_offset_of_m_Operator_1(),
	TriggerRule_t1946298321::get_offset_of_m_Value_2(),
	TriggerRule_t1946298321::get_offset_of_m_Value2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (U3CModuleU3E_t692745546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (Camera_t4174856228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[4] = 
{
	Camera_t4174856228::get_offset_of_hero_2(),
	Camera_t4174856228::get_offset_of_pos_3(),
	Camera_t4174856228::get_offset_of_coord_4(),
	Camera_t4174856228::get_offset_of_game_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (CheckerController_t1724959013), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (GameController_t2330501625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1811[10] = 
{
	GameController_t2330501625::get_offset_of_scoreText_2(),
	GameController_t2330501625::get_offset_of_deathScore_3(),
	GameController_t2330501625::get_offset_of_record_4(),
	GameController_t2330501625::get_offset_of_hero_5(),
	GameController_t2330501625::get_offset_of_GameGUI_6(),
	GameController_t2330501625::get_offset_of_StartGUI_7(),
	GameController_t2330501625::get_offset_of_SettingsGUI_8(),
	GameController_t2330501625::get_offset_of_DeathGUI_9(),
	GameController_t2330501625::get_offset_of_mainCamera_10(),
	GameController_t2330501625::get_offset_of_scene_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (GateController_t2541762910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1812[5] = 
{
	GateController_t2541762910::get_offset_of_player_2(),
	GateController_t2541762910::get_offset_of_hero_3(),
	GateController_t2541762910::get_offset_of_sprites_4(),
	GateController_t2541762910::get_offset_of_colorGate_5(),
	GateController_t2541762910::get_offset_of_shapeGate_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (HeroController_t1470947597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1813[23] = 
{
	HeroController_t1470947597::get_offset_of_speed_2(),
	HeroController_t1470947597::get_offset_of_jumpForce_3(),
	HeroController_t1470947597::get_offset_of_maxSpeed_4(),
	HeroController_t1470947597::get_offset_of_increaseSpeed_5(),
	HeroController_t1470947597::get_offset_of_yelCirc_6(),
	HeroController_t1470947597::get_offset_of_yelClock_7(),
	HeroController_t1470947597::get_offset_of_yelRhomb_8(),
	HeroController_t1470947597::get_offset_of_redCirc_9(),
	HeroController_t1470947597::get_offset_of_redClock_10(),
	HeroController_t1470947597::get_offset_of_redRhomb_11(),
	HeroController_t1470947597::get_offset_of_violetCirc_12(),
	HeroController_t1470947597::get_offset_of_violetClock_13(),
	HeroController_t1470947597::get_offset_of_violetRhomb_14(),
	HeroController_t1470947597::get_offset_of_isMove_15(),
	HeroController_t1470947597::get_offset_of_cameraStart_16(),
	HeroController_t1470947597::get_offset_of_isDeath_17(),
	HeroController_t1470947597::get_offset_of_isGrounded_18(),
	HeroController_t1470947597::get_offset_of_isJump_19(),
	HeroController_t1470947597::get_offset_of_heroSprite_20(),
	HeroController_t1470947597::get_offset_of_rb_21(),
	HeroController_t1470947597::get_offset_of_sprites_22(),
	HeroController_t1470947597::get_offset_of_color_23(),
	HeroController_t1470947597::get_offset_of_shape_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (JumpZone_t2032044937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1814[2] = 
{
	JumpZone_t2032044937::get_offset_of_player_2(),
	JumpZone_t2032044937::get_offset_of_hero_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (LevelController_t24418946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[6] = 
{
	LevelController_t24418946::get_offset_of_platformPrefab_2(),
	LevelController_t24418946::get_offset_of_startPosition_3(),
	LevelController_t24418946::get_offset_of_countPlatform_4(),
	LevelController_t24418946::get_offset_of_changePos_5(),
	LevelController_t24418946::get_offset_of_platforms_6(),
	LevelController_t24418946::get_offset_of_localScore_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (PlatformColliderController_t1042155798), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1816[2] = 
{
	PlatformColliderController_t1042155798::get_offset_of_player_2(),
	PlatformColliderController_t1042155798::get_offset_of_hero_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (PlatformController_t389759191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[6] = 
{
	PlatformController_t389759191::get_offset_of_gates_2(),
	PlatformController_t389759191::get_offset_of_instGates_3(),
	PlatformController_t389759191::get_offset_of_groundPrefab_4(),
	PlatformController_t389759191::get_offset_of_ground_5(),
	PlatformController_t389759191::get_offset_of_positions_6(),
	PlatformController_t389759191::get_offset_of_positionGround_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
