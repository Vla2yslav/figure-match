﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// Camera
struct Camera_t4174856228;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Transform
struct Transform_t3600365921;
// HeroController
struct HeroController_t1470947597;
// CheckerController
struct CheckerController_t1724959013;
// System.String
struct String_t;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// GameController
struct GameController_t2330501625;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// GateController
struct GateController_t2541762910;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t911335936;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t1693969295;
// JumpZone
struct JumpZone_t2032044937;
// LevelController
struct LevelController_t24418946;
// PlatformController
struct PlatformController_t389759191;
// PlatformColliderController
struct PlatformColliderController_t1042155798;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Object
struct Object_t631007953;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// UnityEngine.AudioSourceExtension
struct AudioSourceExtension_t3064908834;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Sprite[0...,0...]
struct SpriteU5B0___U2C0___U5D_t2581906350;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.UI.FontData
struct FontData_t746620069;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;

extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern const uint32_t Camera_Update_m2029487390_MetadataUsageId;
extern String_t* _stringLiteral3921837499;
extern String_t* _stringLiteral2281897856;
extern const uint32_t CheckerController_Start_m1291968488_MetadataUsageId;
extern const uint32_t CheckerController_OnTriggerEnter2D_m1874730116_MetadataUsageId;
extern String_t* _stringLiteral1512031223;
extern String_t* _stringLiteral2002597352;
extern String_t* _stringLiteral3454842817;
extern String_t* _stringLiteral3964750823;
extern const uint32_t GameController_Awake_m4256893697_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3502151501;
extern String_t* _stringLiteral3070507005;
extern const uint32_t GameController_Update_m217025042_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var;
extern const uint32_t GameController_Play_m3493558509_MetadataUsageId;
extern const uint32_t GameController_OpenSettings_m1148998156_MetadataUsageId;
extern const uint32_t GameController_OpenStart_m1751541339_MetadataUsageId;
extern const uint32_t GameController_RateApp_m44342360_MetadataUsageId;
extern const uint32_t GameController_Home_m2940333881_MetadataUsageId;
extern const uint32_t GameController_Restart_m3616958909_MetadataUsageId;
extern String_t* _stringLiteral133445101;
extern const uint32_t GameController_Sound_m1311354126_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisAudioSource_t3935305588_m625814604_RuntimeMethod_var;
extern const uint32_t GameController_Music_m2239397093_MetadataUsageId;
extern const uint32_t GameController_RestorePurchases_m91018808_MetadataUsageId;
extern const uint32_t GameController_Leaderboard_m3839236744_MetadataUsageId;
extern const uint32_t GameController_AD_m3177971801_MetadataUsageId;
extern RuntimeClass* SpriteRendererU5BU5D_t911335936_il2cpp_TypeInfo_var;
extern const uint32_t GateController__ctor_m600729373_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisHeroController_t1470947597_m1641473813_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponentsInChildren_TisSpriteRenderer_t3235626157_m3632004692_RuntimeMethod_var;
extern String_t* _stringLiteral1256528376;
extern const uint32_t GateController_Start_m1837277945_MetadataUsageId;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral2261822918;
extern String_t* _stringLiteral1895510177;
extern const uint32_t GateController_OnTriggerEnter2D_m2264937911_MetadataUsageId;
extern String_t* _stringLiteral3446927122;
extern const uint32_t GateController_OnTriggerStay2D_m408863445_MetadataUsageId;
extern RuntimeClass* SpriteU5B0___U2C0___U5D_t2581906350_il2cpp_TypeInfo_var;
extern const uint32_t HeroController__ctor_m2805344883_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var;
extern const uint32_t HeroController_Start_m163809011_MetadataUsageId;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern const uint32_t HeroController_FixedUpdate_m1083043067_MetadataUsageId;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern const uint32_t HeroController_Update_m2266239483_MetadataUsageId;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern const uint32_t HeroController_Swipe_m2874113322_MetadataUsageId;
extern String_t* _stringLiteral2984908384;
extern const uint32_t HeroController_Move_m2322399700_MetadataUsageId;
extern RuntimeClass* Physics2D_t1528932956_il2cpp_TypeInfo_var;
extern const uint32_t HeroController_CheckGround_m2788034350_MetadataUsageId;
extern const uint32_t JumpZone_Start_m3110681720_MetadataUsageId;
extern const uint32_t JumpZone_OnTriggerEnter2D_m2019572104_MetadataUsageId;
extern RuntimeClass* GameObjectU5BU5D_t3328599146_il2cpp_TypeInfo_var;
extern const uint32_t LevelController__ctor_m3999760241_MetadataUsageId;
extern const RuntimeMethod* Resources_Load_TisGameObject_t1113636619_m1734345100_RuntimeMethod_var;
extern String_t* _stringLiteral38631985;
extern const uint32_t LevelController_Awake_m2998741377_MetadataUsageId;
extern RuntimeClass* Quaternion_t2301928331_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var;
extern const uint32_t LevelController_Start_m3354251513_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisPlatformController_t389759191_m3531744381_RuntimeMethod_var;
extern const uint32_t LevelController_Update_m1452339333_MetadataUsageId;
extern const uint32_t PlatformColliderController_Start_m1491561211_MetadataUsageId;
extern String_t* _stringLiteral4069454954;
extern const uint32_t PlatformColliderController_OnTriggerEnter2D_m3155749451_MetadataUsageId;
extern RuntimeClass* Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Resources_LoadAll_TisGameObject_t1113636619_m2490946171_RuntimeMethod_var;
extern String_t* _stringLiteral78692068;
extern String_t* _stringLiteral3128803744;
extern const uint32_t PlatformController_Awake_m672361463_MetadataUsageId;
extern const uint32_t PlatformController_Start_m1630760214_MetadataUsageId;
extern const uint32_t PlatformController_DeleteGates_m2200208598_MetadataUsageId;
extern const uint32_t PlatformController_CreateGates_m1101393362_MetadataUsageId;

struct SpriteRendererU5BU5D_t911335936;
struct SpriteU5B0___U2C0___U5D_t2581906350;
struct Collider2DU5BU5D_t1693969295;
struct GameObjectU5BU5D_t3328599146;
struct Vector3U5BU5D_t1718750761;


#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef SCENE_T2348375561_H
#define SCENE_T2348375561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t2348375561 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t2348375561, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T2348375561_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef FORCEMODE2D_T255358695_H
#define FORCEMODE2D_T255358695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ForceMode2D
struct  ForceMode2D_t255358695 
{
public:
	// System.Int32 UnityEngine.ForceMode2D::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ForceMode2D_t255358695, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEMODE2D_T255358695_H
#ifndef TOUCHTYPE_T2034578258_H
#define TOUCHTYPE_T2034578258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t2034578258 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t2034578258, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T2034578258_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TOUCHPHASE_T72348083_H
#define TOUCHPHASE_T72348083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t72348083 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t72348083, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T72348083_H
#ifndef TOUCH_T1921856868_H
#define TOUCH_T1921856868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t1921856868 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t2156229523  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t2156229523  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t2156229523  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Position_1)); }
	inline Vector2_t2156229523  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t2156229523 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t2156229523  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RawPosition_2)); }
	inline Vector2_t2156229523  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t2156229523 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t2156229523  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_PositionDelta_3)); }
	inline Vector2_t2156229523  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t2156229523 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t2156229523  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T1921856868_H
#ifndef SPRITE_T280657092_H
#define SPRITE_T280657092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t280657092  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T280657092_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef RIGIDBODY2D_T939494601_H
#define RIGIDBODY2D_T939494601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody2D
struct  Rigidbody2D_t939494601  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY2D_T939494601_H
#ifndef RENDERER_T2627027031_H
#define RENDERER_T2627027031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t2627027031  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T2627027031_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef COLLIDER2D_T2806799626_H
#define COLLIDER2D_T2806799626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_t2806799626  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_T2806799626_H
#ifndef SPRITERENDERER_T3235626157_H
#define SPRITERENDERER_T3235626157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t3235626157  : public Renderer_t2627027031
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITERENDERER_T3235626157_H
#ifndef AUDIOSOURCE_T3935305588_H
#define AUDIOSOURCE_T3935305588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSource
struct  AudioSource_t3935305588  : public Behaviour_t1437897464
{
public:
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::spatializerExtension
	AudioSourceExtension_t3064908834 * ___spatializerExtension_2;
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::ambisonicExtension
	AudioSourceExtension_t3064908834 * ___ambisonicExtension_3;

public:
	inline static int32_t get_offset_of_spatializerExtension_2() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___spatializerExtension_2)); }
	inline AudioSourceExtension_t3064908834 * get_spatializerExtension_2() const { return ___spatializerExtension_2; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_spatializerExtension_2() { return &___spatializerExtension_2; }
	inline void set_spatializerExtension_2(AudioSourceExtension_t3064908834 * value)
	{
		___spatializerExtension_2 = value;
		Il2CppCodeGenWriteBarrier((&___spatializerExtension_2), value);
	}

	inline static int32_t get_offset_of_ambisonicExtension_3() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___ambisonicExtension_3)); }
	inline AudioSourceExtension_t3064908834 * get_ambisonicExtension_3() const { return ___ambisonicExtension_3; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_ambisonicExtension_3() { return &___ambisonicExtension_3; }
	inline void set_ambisonicExtension_3(AudioSourceExtension_t3064908834 * value)
	{
		___ambisonicExtension_3 = value;
		Il2CppCodeGenWriteBarrier((&___ambisonicExtension_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCE_T3935305588_H
#ifndef PLATFORMCOLLIDERCONTROLLER_T1042155798_H
#define PLATFORMCOLLIDERCONTROLLER_T1042155798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlatformColliderController
struct  PlatformColliderController_t1042155798  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PlatformColliderController::player
	GameObject_t1113636619 * ___player_2;
	// HeroController PlatformColliderController::hero
	HeroController_t1470947597 * ___hero_3;

public:
	inline static int32_t get_offset_of_player_2() { return static_cast<int32_t>(offsetof(PlatformColliderController_t1042155798, ___player_2)); }
	inline GameObject_t1113636619 * get_player_2() const { return ___player_2; }
	inline GameObject_t1113636619 ** get_address_of_player_2() { return &___player_2; }
	inline void set_player_2(GameObject_t1113636619 * value)
	{
		___player_2 = value;
		Il2CppCodeGenWriteBarrier((&___player_2), value);
	}

	inline static int32_t get_offset_of_hero_3() { return static_cast<int32_t>(offsetof(PlatformColliderController_t1042155798, ___hero_3)); }
	inline HeroController_t1470947597 * get_hero_3() const { return ___hero_3; }
	inline HeroController_t1470947597 ** get_address_of_hero_3() { return &___hero_3; }
	inline void set_hero_3(HeroController_t1470947597 * value)
	{
		___hero_3 = value;
		Il2CppCodeGenWriteBarrier((&___hero_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMCOLLIDERCONTROLLER_T1042155798_H
#ifndef CAMERA_T4174856228_H
#define CAMERA_T4174856228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Camera
struct  Camera_t4174856228  : public MonoBehaviour_t3962482529
{
public:
	// HeroController Camera::hero
	HeroController_t1470947597 * ___hero_2;
	// UnityEngine.Vector3 Camera::pos
	Vector3_t3722313464  ___pos_3;
	// UnityEngine.Vector3 Camera::coord
	Vector3_t3722313464  ___coord_4;
	// System.Boolean Camera::game
	bool ___game_5;

public:
	inline static int32_t get_offset_of_hero_2() { return static_cast<int32_t>(offsetof(Camera_t4174856228, ___hero_2)); }
	inline HeroController_t1470947597 * get_hero_2() const { return ___hero_2; }
	inline HeroController_t1470947597 ** get_address_of_hero_2() { return &___hero_2; }
	inline void set_hero_2(HeroController_t1470947597 * value)
	{
		___hero_2 = value;
		Il2CppCodeGenWriteBarrier((&___hero_2), value);
	}

	inline static int32_t get_offset_of_pos_3() { return static_cast<int32_t>(offsetof(Camera_t4174856228, ___pos_3)); }
	inline Vector3_t3722313464  get_pos_3() const { return ___pos_3; }
	inline Vector3_t3722313464 * get_address_of_pos_3() { return &___pos_3; }
	inline void set_pos_3(Vector3_t3722313464  value)
	{
		___pos_3 = value;
	}

	inline static int32_t get_offset_of_coord_4() { return static_cast<int32_t>(offsetof(Camera_t4174856228, ___coord_4)); }
	inline Vector3_t3722313464  get_coord_4() const { return ___coord_4; }
	inline Vector3_t3722313464 * get_address_of_coord_4() { return &___coord_4; }
	inline void set_coord_4(Vector3_t3722313464  value)
	{
		___coord_4 = value;
	}

	inline static int32_t get_offset_of_game_5() { return static_cast<int32_t>(offsetof(Camera_t4174856228, ___game_5)); }
	inline bool get_game_5() const { return ___game_5; }
	inline bool* get_address_of_game_5() { return &___game_5; }
	inline void set_game_5(bool value)
	{
		___game_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T4174856228_H
#ifndef CHECKERCONTROLLER_T1724959013_H
#define CHECKERCONTROLLER_T1724959013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CheckerController
struct  CheckerController_t1724959013  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKERCONTROLLER_T1724959013_H
#ifndef GATECONTROLLER_T2541762910_H
#define GATECONTROLLER_T2541762910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GateController
struct  GateController_t2541762910  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject GateController::player
	GameObject_t1113636619 * ___player_2;
	// HeroController GateController::hero
	HeroController_t1470947597 * ___hero_3;
	// UnityEngine.SpriteRenderer[] GateController::sprites
	SpriteRendererU5BU5D_t911335936* ___sprites_4;
	// System.Int32 GateController::colorGate
	int32_t ___colorGate_5;
	// System.Int32 GateController::shapeGate
	int32_t ___shapeGate_6;

public:
	inline static int32_t get_offset_of_player_2() { return static_cast<int32_t>(offsetof(GateController_t2541762910, ___player_2)); }
	inline GameObject_t1113636619 * get_player_2() const { return ___player_2; }
	inline GameObject_t1113636619 ** get_address_of_player_2() { return &___player_2; }
	inline void set_player_2(GameObject_t1113636619 * value)
	{
		___player_2 = value;
		Il2CppCodeGenWriteBarrier((&___player_2), value);
	}

	inline static int32_t get_offset_of_hero_3() { return static_cast<int32_t>(offsetof(GateController_t2541762910, ___hero_3)); }
	inline HeroController_t1470947597 * get_hero_3() const { return ___hero_3; }
	inline HeroController_t1470947597 ** get_address_of_hero_3() { return &___hero_3; }
	inline void set_hero_3(HeroController_t1470947597 * value)
	{
		___hero_3 = value;
		Il2CppCodeGenWriteBarrier((&___hero_3), value);
	}

	inline static int32_t get_offset_of_sprites_4() { return static_cast<int32_t>(offsetof(GateController_t2541762910, ___sprites_4)); }
	inline SpriteRendererU5BU5D_t911335936* get_sprites_4() const { return ___sprites_4; }
	inline SpriteRendererU5BU5D_t911335936** get_address_of_sprites_4() { return &___sprites_4; }
	inline void set_sprites_4(SpriteRendererU5BU5D_t911335936* value)
	{
		___sprites_4 = value;
		Il2CppCodeGenWriteBarrier((&___sprites_4), value);
	}

	inline static int32_t get_offset_of_colorGate_5() { return static_cast<int32_t>(offsetof(GateController_t2541762910, ___colorGate_5)); }
	inline int32_t get_colorGate_5() const { return ___colorGate_5; }
	inline int32_t* get_address_of_colorGate_5() { return &___colorGate_5; }
	inline void set_colorGate_5(int32_t value)
	{
		___colorGate_5 = value;
	}

	inline static int32_t get_offset_of_shapeGate_6() { return static_cast<int32_t>(offsetof(GateController_t2541762910, ___shapeGate_6)); }
	inline int32_t get_shapeGate_6() const { return ___shapeGate_6; }
	inline int32_t* get_address_of_shapeGate_6() { return &___shapeGate_6; }
	inline void set_shapeGate_6(int32_t value)
	{
		___shapeGate_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GATECONTROLLER_T2541762910_H
#ifndef GAMECONTROLLER_T2330501625_H
#define GAMECONTROLLER_T2330501625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController
struct  GameController_t2330501625  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text GameController::scoreText
	Text_t1901882714 * ___scoreText_2;
	// UnityEngine.UI.Text GameController::deathScore
	Text_t1901882714 * ___deathScore_3;
	// UnityEngine.UI.Text GameController::record
	Text_t1901882714 * ___record_4;
	// HeroController GameController::hero
	HeroController_t1470947597 * ___hero_5;
	// UnityEngine.GameObject GameController::GameGUI
	GameObject_t1113636619 * ___GameGUI_6;
	// UnityEngine.GameObject GameController::StartGUI
	GameObject_t1113636619 * ___StartGUI_7;
	// UnityEngine.GameObject GameController::SettingsGUI
	GameObject_t1113636619 * ___SettingsGUI_8;
	// UnityEngine.GameObject GameController::DeathGUI
	GameObject_t1113636619 * ___DeathGUI_9;
	// UnityEngine.GameObject GameController::mainCamera
	GameObject_t1113636619 * ___mainCamera_10;
	// UnityEngine.SceneManagement.Scene GameController::scene
	Scene_t2348375561  ___scene_11;

public:
	inline static int32_t get_offset_of_scoreText_2() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___scoreText_2)); }
	inline Text_t1901882714 * get_scoreText_2() const { return ___scoreText_2; }
	inline Text_t1901882714 ** get_address_of_scoreText_2() { return &___scoreText_2; }
	inline void set_scoreText_2(Text_t1901882714 * value)
	{
		___scoreText_2 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_2), value);
	}

	inline static int32_t get_offset_of_deathScore_3() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___deathScore_3)); }
	inline Text_t1901882714 * get_deathScore_3() const { return ___deathScore_3; }
	inline Text_t1901882714 ** get_address_of_deathScore_3() { return &___deathScore_3; }
	inline void set_deathScore_3(Text_t1901882714 * value)
	{
		___deathScore_3 = value;
		Il2CppCodeGenWriteBarrier((&___deathScore_3), value);
	}

	inline static int32_t get_offset_of_record_4() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___record_4)); }
	inline Text_t1901882714 * get_record_4() const { return ___record_4; }
	inline Text_t1901882714 ** get_address_of_record_4() { return &___record_4; }
	inline void set_record_4(Text_t1901882714 * value)
	{
		___record_4 = value;
		Il2CppCodeGenWriteBarrier((&___record_4), value);
	}

	inline static int32_t get_offset_of_hero_5() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___hero_5)); }
	inline HeroController_t1470947597 * get_hero_5() const { return ___hero_5; }
	inline HeroController_t1470947597 ** get_address_of_hero_5() { return &___hero_5; }
	inline void set_hero_5(HeroController_t1470947597 * value)
	{
		___hero_5 = value;
		Il2CppCodeGenWriteBarrier((&___hero_5), value);
	}

	inline static int32_t get_offset_of_GameGUI_6() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___GameGUI_6)); }
	inline GameObject_t1113636619 * get_GameGUI_6() const { return ___GameGUI_6; }
	inline GameObject_t1113636619 ** get_address_of_GameGUI_6() { return &___GameGUI_6; }
	inline void set_GameGUI_6(GameObject_t1113636619 * value)
	{
		___GameGUI_6 = value;
		Il2CppCodeGenWriteBarrier((&___GameGUI_6), value);
	}

	inline static int32_t get_offset_of_StartGUI_7() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___StartGUI_7)); }
	inline GameObject_t1113636619 * get_StartGUI_7() const { return ___StartGUI_7; }
	inline GameObject_t1113636619 ** get_address_of_StartGUI_7() { return &___StartGUI_7; }
	inline void set_StartGUI_7(GameObject_t1113636619 * value)
	{
		___StartGUI_7 = value;
		Il2CppCodeGenWriteBarrier((&___StartGUI_7), value);
	}

	inline static int32_t get_offset_of_SettingsGUI_8() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___SettingsGUI_8)); }
	inline GameObject_t1113636619 * get_SettingsGUI_8() const { return ___SettingsGUI_8; }
	inline GameObject_t1113636619 ** get_address_of_SettingsGUI_8() { return &___SettingsGUI_8; }
	inline void set_SettingsGUI_8(GameObject_t1113636619 * value)
	{
		___SettingsGUI_8 = value;
		Il2CppCodeGenWriteBarrier((&___SettingsGUI_8), value);
	}

	inline static int32_t get_offset_of_DeathGUI_9() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___DeathGUI_9)); }
	inline GameObject_t1113636619 * get_DeathGUI_9() const { return ___DeathGUI_9; }
	inline GameObject_t1113636619 ** get_address_of_DeathGUI_9() { return &___DeathGUI_9; }
	inline void set_DeathGUI_9(GameObject_t1113636619 * value)
	{
		___DeathGUI_9 = value;
		Il2CppCodeGenWriteBarrier((&___DeathGUI_9), value);
	}

	inline static int32_t get_offset_of_mainCamera_10() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___mainCamera_10)); }
	inline GameObject_t1113636619 * get_mainCamera_10() const { return ___mainCamera_10; }
	inline GameObject_t1113636619 ** get_address_of_mainCamera_10() { return &___mainCamera_10; }
	inline void set_mainCamera_10(GameObject_t1113636619 * value)
	{
		___mainCamera_10 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_10), value);
	}

	inline static int32_t get_offset_of_scene_11() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___scene_11)); }
	inline Scene_t2348375561  get_scene_11() const { return ___scene_11; }
	inline Scene_t2348375561 * get_address_of_scene_11() { return &___scene_11; }
	inline void set_scene_11(Scene_t2348375561  value)
	{
		___scene_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECONTROLLER_T2330501625_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef HEROCONTROLLER_T1470947597_H
#define HEROCONTROLLER_T1470947597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroController
struct  HeroController_t1470947597  : public MonoBehaviour_t3962482529
{
public:
	// System.Single HeroController::speed
	float ___speed_2;
	// System.Single HeroController::jumpForce
	float ___jumpForce_3;
	// System.Single HeroController::maxSpeed
	float ___maxSpeed_4;
	// System.Single HeroController::increaseSpeed
	float ___increaseSpeed_5;
	// UnityEngine.Sprite HeroController::yelCirc
	Sprite_t280657092 * ___yelCirc_6;
	// UnityEngine.Sprite HeroController::yelClock
	Sprite_t280657092 * ___yelClock_7;
	// UnityEngine.Sprite HeroController::yelRhomb
	Sprite_t280657092 * ___yelRhomb_8;
	// UnityEngine.Sprite HeroController::redCirc
	Sprite_t280657092 * ___redCirc_9;
	// UnityEngine.Sprite HeroController::redClock
	Sprite_t280657092 * ___redClock_10;
	// UnityEngine.Sprite HeroController::redRhomb
	Sprite_t280657092 * ___redRhomb_11;
	// UnityEngine.Sprite HeroController::violetCirc
	Sprite_t280657092 * ___violetCirc_12;
	// UnityEngine.Sprite HeroController::violetClock
	Sprite_t280657092 * ___violetClock_13;
	// UnityEngine.Sprite HeroController::violetRhomb
	Sprite_t280657092 * ___violetRhomb_14;
	// System.Boolean HeroController::isMove
	bool ___isMove_15;
	// System.Boolean HeroController::cameraStart
	bool ___cameraStart_16;
	// System.Boolean HeroController::isDeath
	bool ___isDeath_17;
	// System.Boolean HeroController::isGrounded
	bool ___isGrounded_18;
	// System.Boolean HeroController::isJump
	bool ___isJump_19;
	// UnityEngine.SpriteRenderer HeroController::heroSprite
	SpriteRenderer_t3235626157 * ___heroSprite_20;
	// UnityEngine.Rigidbody2D HeroController::rb
	Rigidbody2D_t939494601 * ___rb_21;
	// UnityEngine.Sprite[0...,0...] HeroController::sprites
	SpriteU5B0___U2C0___U5D_t2581906350* ___sprites_22;
	// System.Int32 HeroController::color
	int32_t ___color_23;
	// System.Int32 HeroController::shape
	int32_t ___shape_24;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_jumpForce_3() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___jumpForce_3)); }
	inline float get_jumpForce_3() const { return ___jumpForce_3; }
	inline float* get_address_of_jumpForce_3() { return &___jumpForce_3; }
	inline void set_jumpForce_3(float value)
	{
		___jumpForce_3 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_4() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___maxSpeed_4)); }
	inline float get_maxSpeed_4() const { return ___maxSpeed_4; }
	inline float* get_address_of_maxSpeed_4() { return &___maxSpeed_4; }
	inline void set_maxSpeed_4(float value)
	{
		___maxSpeed_4 = value;
	}

	inline static int32_t get_offset_of_increaseSpeed_5() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___increaseSpeed_5)); }
	inline float get_increaseSpeed_5() const { return ___increaseSpeed_5; }
	inline float* get_address_of_increaseSpeed_5() { return &___increaseSpeed_5; }
	inline void set_increaseSpeed_5(float value)
	{
		___increaseSpeed_5 = value;
	}

	inline static int32_t get_offset_of_yelCirc_6() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___yelCirc_6)); }
	inline Sprite_t280657092 * get_yelCirc_6() const { return ___yelCirc_6; }
	inline Sprite_t280657092 ** get_address_of_yelCirc_6() { return &___yelCirc_6; }
	inline void set_yelCirc_6(Sprite_t280657092 * value)
	{
		___yelCirc_6 = value;
		Il2CppCodeGenWriteBarrier((&___yelCirc_6), value);
	}

	inline static int32_t get_offset_of_yelClock_7() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___yelClock_7)); }
	inline Sprite_t280657092 * get_yelClock_7() const { return ___yelClock_7; }
	inline Sprite_t280657092 ** get_address_of_yelClock_7() { return &___yelClock_7; }
	inline void set_yelClock_7(Sprite_t280657092 * value)
	{
		___yelClock_7 = value;
		Il2CppCodeGenWriteBarrier((&___yelClock_7), value);
	}

	inline static int32_t get_offset_of_yelRhomb_8() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___yelRhomb_8)); }
	inline Sprite_t280657092 * get_yelRhomb_8() const { return ___yelRhomb_8; }
	inline Sprite_t280657092 ** get_address_of_yelRhomb_8() { return &___yelRhomb_8; }
	inline void set_yelRhomb_8(Sprite_t280657092 * value)
	{
		___yelRhomb_8 = value;
		Il2CppCodeGenWriteBarrier((&___yelRhomb_8), value);
	}

	inline static int32_t get_offset_of_redCirc_9() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___redCirc_9)); }
	inline Sprite_t280657092 * get_redCirc_9() const { return ___redCirc_9; }
	inline Sprite_t280657092 ** get_address_of_redCirc_9() { return &___redCirc_9; }
	inline void set_redCirc_9(Sprite_t280657092 * value)
	{
		___redCirc_9 = value;
		Il2CppCodeGenWriteBarrier((&___redCirc_9), value);
	}

	inline static int32_t get_offset_of_redClock_10() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___redClock_10)); }
	inline Sprite_t280657092 * get_redClock_10() const { return ___redClock_10; }
	inline Sprite_t280657092 ** get_address_of_redClock_10() { return &___redClock_10; }
	inline void set_redClock_10(Sprite_t280657092 * value)
	{
		___redClock_10 = value;
		Il2CppCodeGenWriteBarrier((&___redClock_10), value);
	}

	inline static int32_t get_offset_of_redRhomb_11() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___redRhomb_11)); }
	inline Sprite_t280657092 * get_redRhomb_11() const { return ___redRhomb_11; }
	inline Sprite_t280657092 ** get_address_of_redRhomb_11() { return &___redRhomb_11; }
	inline void set_redRhomb_11(Sprite_t280657092 * value)
	{
		___redRhomb_11 = value;
		Il2CppCodeGenWriteBarrier((&___redRhomb_11), value);
	}

	inline static int32_t get_offset_of_violetCirc_12() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___violetCirc_12)); }
	inline Sprite_t280657092 * get_violetCirc_12() const { return ___violetCirc_12; }
	inline Sprite_t280657092 ** get_address_of_violetCirc_12() { return &___violetCirc_12; }
	inline void set_violetCirc_12(Sprite_t280657092 * value)
	{
		___violetCirc_12 = value;
		Il2CppCodeGenWriteBarrier((&___violetCirc_12), value);
	}

	inline static int32_t get_offset_of_violetClock_13() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___violetClock_13)); }
	inline Sprite_t280657092 * get_violetClock_13() const { return ___violetClock_13; }
	inline Sprite_t280657092 ** get_address_of_violetClock_13() { return &___violetClock_13; }
	inline void set_violetClock_13(Sprite_t280657092 * value)
	{
		___violetClock_13 = value;
		Il2CppCodeGenWriteBarrier((&___violetClock_13), value);
	}

	inline static int32_t get_offset_of_violetRhomb_14() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___violetRhomb_14)); }
	inline Sprite_t280657092 * get_violetRhomb_14() const { return ___violetRhomb_14; }
	inline Sprite_t280657092 ** get_address_of_violetRhomb_14() { return &___violetRhomb_14; }
	inline void set_violetRhomb_14(Sprite_t280657092 * value)
	{
		___violetRhomb_14 = value;
		Il2CppCodeGenWriteBarrier((&___violetRhomb_14), value);
	}

	inline static int32_t get_offset_of_isMove_15() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___isMove_15)); }
	inline bool get_isMove_15() const { return ___isMove_15; }
	inline bool* get_address_of_isMove_15() { return &___isMove_15; }
	inline void set_isMove_15(bool value)
	{
		___isMove_15 = value;
	}

	inline static int32_t get_offset_of_cameraStart_16() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___cameraStart_16)); }
	inline bool get_cameraStart_16() const { return ___cameraStart_16; }
	inline bool* get_address_of_cameraStart_16() { return &___cameraStart_16; }
	inline void set_cameraStart_16(bool value)
	{
		___cameraStart_16 = value;
	}

	inline static int32_t get_offset_of_isDeath_17() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___isDeath_17)); }
	inline bool get_isDeath_17() const { return ___isDeath_17; }
	inline bool* get_address_of_isDeath_17() { return &___isDeath_17; }
	inline void set_isDeath_17(bool value)
	{
		___isDeath_17 = value;
	}

	inline static int32_t get_offset_of_isGrounded_18() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___isGrounded_18)); }
	inline bool get_isGrounded_18() const { return ___isGrounded_18; }
	inline bool* get_address_of_isGrounded_18() { return &___isGrounded_18; }
	inline void set_isGrounded_18(bool value)
	{
		___isGrounded_18 = value;
	}

	inline static int32_t get_offset_of_isJump_19() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___isJump_19)); }
	inline bool get_isJump_19() const { return ___isJump_19; }
	inline bool* get_address_of_isJump_19() { return &___isJump_19; }
	inline void set_isJump_19(bool value)
	{
		___isJump_19 = value;
	}

	inline static int32_t get_offset_of_heroSprite_20() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___heroSprite_20)); }
	inline SpriteRenderer_t3235626157 * get_heroSprite_20() const { return ___heroSprite_20; }
	inline SpriteRenderer_t3235626157 ** get_address_of_heroSprite_20() { return &___heroSprite_20; }
	inline void set_heroSprite_20(SpriteRenderer_t3235626157 * value)
	{
		___heroSprite_20 = value;
		Il2CppCodeGenWriteBarrier((&___heroSprite_20), value);
	}

	inline static int32_t get_offset_of_rb_21() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___rb_21)); }
	inline Rigidbody2D_t939494601 * get_rb_21() const { return ___rb_21; }
	inline Rigidbody2D_t939494601 ** get_address_of_rb_21() { return &___rb_21; }
	inline void set_rb_21(Rigidbody2D_t939494601 * value)
	{
		___rb_21 = value;
		Il2CppCodeGenWriteBarrier((&___rb_21), value);
	}

	inline static int32_t get_offset_of_sprites_22() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___sprites_22)); }
	inline SpriteU5B0___U2C0___U5D_t2581906350* get_sprites_22() const { return ___sprites_22; }
	inline SpriteU5B0___U2C0___U5D_t2581906350** get_address_of_sprites_22() { return &___sprites_22; }
	inline void set_sprites_22(SpriteU5B0___U2C0___U5D_t2581906350* value)
	{
		___sprites_22 = value;
		Il2CppCodeGenWriteBarrier((&___sprites_22), value);
	}

	inline static int32_t get_offset_of_color_23() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___color_23)); }
	inline int32_t get_color_23() const { return ___color_23; }
	inline int32_t* get_address_of_color_23() { return &___color_23; }
	inline void set_color_23(int32_t value)
	{
		___color_23 = value;
	}

	inline static int32_t get_offset_of_shape_24() { return static_cast<int32_t>(offsetof(HeroController_t1470947597, ___shape_24)); }
	inline int32_t get_shape_24() const { return ___shape_24; }
	inline int32_t* get_address_of_shape_24() { return &___shape_24; }
	inline void set_shape_24(int32_t value)
	{
		___shape_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROCONTROLLER_T1470947597_H
#ifndef JUMPZONE_T2032044937_H
#define JUMPZONE_T2032044937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JumpZone
struct  JumpZone_t2032044937  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject JumpZone::player
	GameObject_t1113636619 * ___player_2;
	// HeroController JumpZone::hero
	HeroController_t1470947597 * ___hero_3;

public:
	inline static int32_t get_offset_of_player_2() { return static_cast<int32_t>(offsetof(JumpZone_t2032044937, ___player_2)); }
	inline GameObject_t1113636619 * get_player_2() const { return ___player_2; }
	inline GameObject_t1113636619 ** get_address_of_player_2() { return &___player_2; }
	inline void set_player_2(GameObject_t1113636619 * value)
	{
		___player_2 = value;
		Il2CppCodeGenWriteBarrier((&___player_2), value);
	}

	inline static int32_t get_offset_of_hero_3() { return static_cast<int32_t>(offsetof(JumpZone_t2032044937, ___hero_3)); }
	inline HeroController_t1470947597 * get_hero_3() const { return ___hero_3; }
	inline HeroController_t1470947597 ** get_address_of_hero_3() { return &___hero_3; }
	inline void set_hero_3(HeroController_t1470947597 * value)
	{
		___hero_3 = value;
		Il2CppCodeGenWriteBarrier((&___hero_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JUMPZONE_T2032044937_H
#ifndef LEVELCONTROLLER_T24418946_H
#define LEVELCONTROLLER_T24418946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelController
struct  LevelController_t24418946  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject LevelController::platformPrefab
	GameObject_t1113636619 * ___platformPrefab_2;
	// UnityEngine.Vector3 LevelController::startPosition
	Vector3_t3722313464  ___startPosition_3;
	// System.Int32 LevelController::countPlatform
	int32_t ___countPlatform_4;
	// System.Int32 LevelController::changePos
	int32_t ___changePos_5;
	// UnityEngine.GameObject[] LevelController::platforms
	GameObjectU5BU5D_t3328599146* ___platforms_6;
	// System.Int32 LevelController::localScore
	int32_t ___localScore_7;

public:
	inline static int32_t get_offset_of_platformPrefab_2() { return static_cast<int32_t>(offsetof(LevelController_t24418946, ___platformPrefab_2)); }
	inline GameObject_t1113636619 * get_platformPrefab_2() const { return ___platformPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_platformPrefab_2() { return &___platformPrefab_2; }
	inline void set_platformPrefab_2(GameObject_t1113636619 * value)
	{
		___platformPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___platformPrefab_2), value);
	}

	inline static int32_t get_offset_of_startPosition_3() { return static_cast<int32_t>(offsetof(LevelController_t24418946, ___startPosition_3)); }
	inline Vector3_t3722313464  get_startPosition_3() const { return ___startPosition_3; }
	inline Vector3_t3722313464 * get_address_of_startPosition_3() { return &___startPosition_3; }
	inline void set_startPosition_3(Vector3_t3722313464  value)
	{
		___startPosition_3 = value;
	}

	inline static int32_t get_offset_of_countPlatform_4() { return static_cast<int32_t>(offsetof(LevelController_t24418946, ___countPlatform_4)); }
	inline int32_t get_countPlatform_4() const { return ___countPlatform_4; }
	inline int32_t* get_address_of_countPlatform_4() { return &___countPlatform_4; }
	inline void set_countPlatform_4(int32_t value)
	{
		___countPlatform_4 = value;
	}

	inline static int32_t get_offset_of_changePos_5() { return static_cast<int32_t>(offsetof(LevelController_t24418946, ___changePos_5)); }
	inline int32_t get_changePos_5() const { return ___changePos_5; }
	inline int32_t* get_address_of_changePos_5() { return &___changePos_5; }
	inline void set_changePos_5(int32_t value)
	{
		___changePos_5 = value;
	}

	inline static int32_t get_offset_of_platforms_6() { return static_cast<int32_t>(offsetof(LevelController_t24418946, ___platforms_6)); }
	inline GameObjectU5BU5D_t3328599146* get_platforms_6() const { return ___platforms_6; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_platforms_6() { return &___platforms_6; }
	inline void set_platforms_6(GameObjectU5BU5D_t3328599146* value)
	{
		___platforms_6 = value;
		Il2CppCodeGenWriteBarrier((&___platforms_6), value);
	}

	inline static int32_t get_offset_of_localScore_7() { return static_cast<int32_t>(offsetof(LevelController_t24418946, ___localScore_7)); }
	inline int32_t get_localScore_7() const { return ___localScore_7; }
	inline int32_t* get_address_of_localScore_7() { return &___localScore_7; }
	inline void set_localScore_7(int32_t value)
	{
		___localScore_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCONTROLLER_T24418946_H
#ifndef PLATFORMCONTROLLER_T389759191_H
#define PLATFORMCONTROLLER_T389759191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlatformController
struct  PlatformController_t389759191  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] PlatformController::gates
	GameObjectU5BU5D_t3328599146* ___gates_2;
	// UnityEngine.GameObject[] PlatformController::instGates
	GameObjectU5BU5D_t3328599146* ___instGates_3;
	// UnityEngine.GameObject PlatformController::groundPrefab
	GameObject_t1113636619 * ___groundPrefab_4;
	// UnityEngine.GameObject PlatformController::ground
	GameObject_t1113636619 * ___ground_5;
	// UnityEngine.Vector3[] PlatformController::positions
	Vector3U5BU5D_t1718750761* ___positions_6;
	// UnityEngine.Vector3 PlatformController::positionGround
	Vector3_t3722313464  ___positionGround_7;

public:
	inline static int32_t get_offset_of_gates_2() { return static_cast<int32_t>(offsetof(PlatformController_t389759191, ___gates_2)); }
	inline GameObjectU5BU5D_t3328599146* get_gates_2() const { return ___gates_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_gates_2() { return &___gates_2; }
	inline void set_gates_2(GameObjectU5BU5D_t3328599146* value)
	{
		___gates_2 = value;
		Il2CppCodeGenWriteBarrier((&___gates_2), value);
	}

	inline static int32_t get_offset_of_instGates_3() { return static_cast<int32_t>(offsetof(PlatformController_t389759191, ___instGates_3)); }
	inline GameObjectU5BU5D_t3328599146* get_instGates_3() const { return ___instGates_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_instGates_3() { return &___instGates_3; }
	inline void set_instGates_3(GameObjectU5BU5D_t3328599146* value)
	{
		___instGates_3 = value;
		Il2CppCodeGenWriteBarrier((&___instGates_3), value);
	}

	inline static int32_t get_offset_of_groundPrefab_4() { return static_cast<int32_t>(offsetof(PlatformController_t389759191, ___groundPrefab_4)); }
	inline GameObject_t1113636619 * get_groundPrefab_4() const { return ___groundPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_groundPrefab_4() { return &___groundPrefab_4; }
	inline void set_groundPrefab_4(GameObject_t1113636619 * value)
	{
		___groundPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___groundPrefab_4), value);
	}

	inline static int32_t get_offset_of_ground_5() { return static_cast<int32_t>(offsetof(PlatformController_t389759191, ___ground_5)); }
	inline GameObject_t1113636619 * get_ground_5() const { return ___ground_5; }
	inline GameObject_t1113636619 ** get_address_of_ground_5() { return &___ground_5; }
	inline void set_ground_5(GameObject_t1113636619 * value)
	{
		___ground_5 = value;
		Il2CppCodeGenWriteBarrier((&___ground_5), value);
	}

	inline static int32_t get_offset_of_positions_6() { return static_cast<int32_t>(offsetof(PlatformController_t389759191, ___positions_6)); }
	inline Vector3U5BU5D_t1718750761* get_positions_6() const { return ___positions_6; }
	inline Vector3U5BU5D_t1718750761** get_address_of_positions_6() { return &___positions_6; }
	inline void set_positions_6(Vector3U5BU5D_t1718750761* value)
	{
		___positions_6 = value;
		Il2CppCodeGenWriteBarrier((&___positions_6), value);
	}

	inline static int32_t get_offset_of_positionGround_7() { return static_cast<int32_t>(offsetof(PlatformController_t389759191, ___positionGround_7)); }
	inline Vector3_t3722313464  get_positionGround_7() const { return ___positionGround_7; }
	inline Vector3_t3722313464 * get_address_of_positionGround_7() { return &___positionGround_7; }
	inline void set_positionGround_7(Vector3_t3722313464  value)
	{
		___positionGround_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMCONTROLLER_T389759191_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t2598313366 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_28)); }
	inline FontData_t746620069 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t746620069 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t746620069 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_30)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_32)); }
	inline Material_t340375123 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t340375123 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t911335936  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) SpriteRenderer_t3235626157 * m_Items[1];

public:
	inline SpriteRenderer_t3235626157 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SpriteRenderer_t3235626157 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SpriteRenderer_t3235626157 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SpriteRenderer_t3235626157 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SpriteRenderer_t3235626157 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SpriteRenderer_t3235626157 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Sprite[0...,0...]
struct SpriteU5B0___U2C0___U5D_t2581906350  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Sprite_t280657092 * m_Items[1];

public:
	inline Sprite_t280657092 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Sprite_t280657092 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Sprite_t280657092 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Sprite_t280657092 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Sprite_t280657092 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Sprite_t280657092 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Sprite_t280657092 * GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline Sprite_t280657092 ** GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, Sprite_t280657092 * value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Sprite_t280657092 * GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline Sprite_t280657092 ** GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, Sprite_t280657092 * value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t1693969295  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Collider2D_t2806799626 * m_Items[1];

public:
	inline Collider2D_t2806799626 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Collider2D_t2806799626 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Collider2D_t2806799626 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Collider2D_t2806799626 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Collider2D_t2806799626 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Collider2D_t2806799626 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_t1113636619 * m_Items[1];

public:
	inline GameObject_t1113636619 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1113636619 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t3722313464  m_Items[1];

public:
	inline Vector3_t3722313464  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t3722313464  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t2843939325* GameObject_GetComponentsInChildren_TisRuntimeObject_m1319386616_gshared (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
extern "C"  RuntimeObject * Resources_Load_TisRuntimeObject_m597869152_gshared (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  RuntimeObject * Object_Instantiate_TisRuntimeObject_m1135049463_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Vector3_t3722313464  p1, Quaternion_t2301928331  p2, const RuntimeMethod* method);
// !!0[] UnityEngine.Resources::LoadAll<System.Object>(System.String)
extern "C"  ObjectU5BU5D_t2843939325* Resources_LoadAll_TisRuntimeObject_m3261828702_gshared (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroController::GetCameraStart()
extern "C"  bool HeroController_GetCameraStart_m2480587098 (HeroController_t1470947597 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::MoveTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_MoveTowards_m2786395547 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Equality_m4231250055 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HeroController::SetMove(System.Boolean)
extern "C"  void HeroController_SetMove_m3841425258 (HeroController_t1470947597 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_Lerp_m407887542 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern "C"  void PlayerPrefs_SetInt_m2842000469 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
extern "C"  int32_t PlayerPrefs_GetInt_m3797620966 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
extern "C"  void PlayerPrefs_SetString_m2101271233 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t2348375561  SceneManager_GetActiveScene_m1825203488 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
extern "C"  int32_t Scene_get_buildIndex_m270272723 (Scene_t2348375561 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HeroController::SetCameraStart(System.Boolean)
extern "C"  void HeroController_SetCameraStart_m2621664460 (HeroController_t1470947597 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m141394615 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroController::GetDeath()
extern "C"  bool HeroController_GetDeath_m1645202901 (HeroController_t1470947597 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.PlayerPrefs::GetString(System.String)
extern "C"  String_t* PlayerPrefs_GetString_m389913383 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
#define Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, method) ((  AudioSource_t3935305588 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void UnityEngine.AudioSource::Play()
extern "C"  void AudioSource_Play_m48294159 (AudioSource_t3935305588 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
extern "C"  void SceneManager_LoadScene_m3463216446 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AudioSource>()
#define GameObject_GetComponent_TisAudioSource_t3935305588_m625814604(__this, method) ((  AudioSource_t3935305588 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void UnityEngine.AudioSource::Stop()
extern "C"  void AudioSource_Stop_m2682712816 (AudioSource_t3935305588 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1113636619 * GameObject_Find_m2032535176 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<HeroController>()
#define GameObject_GetComponent_TisHeroController_t1470947597_m1641473813(__this, method) ((  HeroController_t1470947597 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.SpriteRenderer>(System.Boolean)
#define GameObject_GetComponentsInChildren_TisSpriteRenderer_t3235626157_m3632004692(__this, p0, method) ((  SpriteRendererU5BU5D_t911335936* (*) (GameObject_t1113636619 *, bool, const RuntimeMethod*))GameObject_GetComponentsInChildren_TisRuntimeObject_m1319386616_gshared)(__this, p0, method)
// System.Boolean UnityEngine.Component::CompareTag(System.String)
extern "C"  bool Component_CompareTag_m1328479619 (Component_t1923634451 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroController::GetShape()
extern "C"  int32_t HeroController_GetShape_m2950644854 (HeroController_t1470947597 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroController::GetColor()
extern "C"  int32_t HeroController_GetColor_m1048228322 (HeroController_t1470947597 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HeroController::IncreaseSpeed()
extern "C"  void HeroController_IncreaseSpeed_m2256340442 (HeroController_t1470947597 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HeroController::SetDeath(System.Boolean)
extern "C"  void HeroController_SetDeath_m3098300077 (HeroController_t1470947597 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroController::GetMove()
extern "C"  bool HeroController_GetMove_m3598639959 (HeroController_t1470947597 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C"  void Renderer_set_enabled_m1727253150 (Renderer_t2627027031 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502(__this, method) ((  SpriteRenderer_t3235626157 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
#define Component_GetComponent_TisRigidbody2D_t939494601_m1531613439(__this, method) ((  Rigidbody2D_t939494601 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void HeroController::CheckGround()
extern "C"  void HeroController_CheckGround_m2788034350 (HeroController_t1470947597 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
extern "C"  Vector2_t2156229523  Rigidbody2D_get_velocity_m366589732 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_magnitude()
extern "C"  float Vector2_get_magnitude_m2752892833 (Vector2_t2156229523 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
extern "C"  Vector2_t2156229523  Vector2_get_normalized_m2683665860 (Vector2_t2156229523 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2156229523  Vector2_op_Multiply_m2347887432 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_set_velocity_m2898400508 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C"  int32_t Input_get_touchCount_m3403849067 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HeroController::Swipe()
extern "C"  void HeroController_Swipe_m2874113322 (HeroController_t1470947597 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
extern "C"  Vector3_t3722313464  Transform_get_right_m2535262102 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Multiply_m3376773913 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Addition_m779775034 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C"  Touch_t1921856868  Input_GetTouch_m2192712756 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_deltaPosition()
extern "C"  Vector2_t2156229523  Touch_get_deltaPosition_m2389653382 (Touch_t1921856868 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
extern "C"  Vector3_t3722313464  Transform_get_up_m3972993886 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Sign(System.Single)
extern "C"  float Mathf_Sign_m3457838305 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2156229523  Vector2_op_Implicit_m4260192859 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_AddForce_m1099013366 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523  p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Input::GetAxis(System.String)
extern "C"  float Input_GetAxis_m4009438427 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
extern "C"  void SpriteRenderer_set_sprite_m1286893786 (SpriteRenderer_t3235626157 * __this, Sprite_t280657092 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll(UnityEngine.Vector2,System.Single)
extern "C"  Collider2DU5BU5D_t1693969295* Physics2D_OverlapCircleAll_m841024216 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HeroController::SetIsJump(System.Boolean)
extern "C"  void HeroController_SetIsJump_m358591439 (HeroController_t1470947597 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Resources::Load<UnityEngine.GameObject>(System.String)
#define Resources_Load_TisGameObject_t1113636619_m1734345100(__this /* static, unused */, p0, method) ((  GameObject_t1113636619 * (*) (RuntimeObject * /* static, unused */, String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_m597869152_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t2301928331  Quaternion_get_identity_m3722672781 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1113636619_m3006960551(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1113636619 * (*) (RuntimeObject * /* static, unused */, GameObject_t1113636619 *, Vector3_t3722313464 , Quaternion_t2301928331 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m1135049463_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.GameObject::GetComponent<PlatformController>()
#define GameObject_GetComponent_TisPlatformController_t389759191_m3531744381(__this, method) ((  PlatformController_t389759191 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void PlatformController::DeleteGates()
extern "C"  void PlatformController_DeleteGates_m2200208598 (PlatformController_t389759191 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformController::CreateGates()
extern "C"  void PlatformController_CreateGates_m1101393362 (PlatformController_t389759191 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.Resources::LoadAll<UnityEngine.GameObject>(System.String)
#define Resources_LoadAll_TisGameObject_t1113636619_m2490946171(__this /* static, unused */, p0, method) ((  GameObjectU5BU5D_t3328599146* (*) (RuntimeObject * /* static, unused */, String_t*, const RuntimeMethod*))Resources_LoadAll_TisRuntimeObject_m3261828702_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C"  void Transform_SetParent_m381167889 (Transform_t3600365921 * __this, Transform_t3600365921 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m4128471975 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m4054026115 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Camera::.ctor()
extern "C"  void Camera__ctor_m770550158 (Camera_t4174856228 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Camera::Start()
extern "C"  void Camera_Start_m2619638368 (Camera_t4174856228 * __this, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		HeroController_t1470947597 * L_0 = __this->get_hero_2();
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		__this->set_pos_3(L_2);
		Vector3_t3722313464 * L_3 = __this->get_address_of_pos_3();
		HeroController_t1470947597 * L_4 = __this->get_hero_2();
		NullCheck(L_4);
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t3722313464  L_6 = Transform_get_position_m36019626(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = (&V_0)->get_x_1();
		L_3->set_x_1(((float)il2cpp_codegen_add((float)L_7, (float)(2.0f))));
		Vector3_t3722313464 * L_8 = __this->get_address_of_pos_3();
		L_8->set_y_2((0.99f));
		Vector3_t3722313464 * L_9 = __this->get_address_of_pos_3();
		L_9->set_z_3((-10.0f));
		Transform_t3600365921 * L_10 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t3722313464  L_11 = Transform_get_position_m36019626(L_10, /*hidden argument*/NULL);
		__this->set_coord_4(L_11);
		return;
	}
}
// System.Void Camera::Update()
extern "C"  void Camera_Update_m2029487390 (Camera_t4174856228 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_Update_m2029487390_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		HeroController_t1470947597 * L_0 = __this->get_hero_2();
		NullCheck(L_0);
		bool L_1 = HeroController_GetCameraStart_m2480587098(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_006a;
		}
	}
	{
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m36019626(L_3, /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = __this->get_pos_3();
		float L_6 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_7 = Vector3_MoveTowards_m2786395547(NULL /*static, unused*/, L_4, L_5, ((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_6)), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m3387557959(L_2, L_7, /*hidden argument*/NULL);
		Transform_t3600365921 * L_8 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t3722313464  L_9 = Transform_get_position_m36019626(L_8, /*hidden argument*/NULL);
		Vector3_t3722313464  L_10 = __this->get_pos_3();
		bool L_11 = Vector3_op_Equality_m4231250055(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_006a;
		}
	}
	{
		HeroController_t1470947597 * L_12 = __this->get_hero_2();
		NullCheck(L_12);
		HeroController_SetMove_m3841425258(L_12, (bool)1, /*hidden argument*/NULL);
		__this->set_game_5((bool)1);
	}

IL_006a:
	{
		bool L_13 = __this->get_game_5();
		if (!L_13)
		{
			goto IL_00e5;
		}
	}
	{
		Transform_t3600365921 * L_14 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t3722313464  L_15 = Transform_get_position_m36019626(L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		(&V_0)->set_y_2((0.99f));
		(&V_0)->set_z_3((-10.0f));
		HeroController_t1470947597 * L_16 = __this->get_hero_2();
		NullCheck(L_16);
		Transform_t3600365921 * L_17 = Component_get_transform_m3162698980(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t3722313464  L_18 = Transform_get_position_m36019626(L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		float L_19 = (&V_1)->get_x_1();
		(&V_0)->set_x_1(((float)il2cpp_codegen_add((float)L_19, (float)(3.5f))));
		Transform_t3600365921 * L_20 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_21 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t3722313464  L_22 = Transform_get_position_m36019626(L_21, /*hidden argument*/NULL);
		Vector3_t3722313464  L_23 = V_0;
		float L_24 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_25 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_22, L_23, ((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_24)), /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_position_m3387557959(L_20, L_25, /*hidden argument*/NULL);
	}

IL_00e5:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CheckerController::.ctor()
extern "C"  void CheckerController__ctor_m3626230912 (CheckerController_t1724959013 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CheckerController::Start()
extern "C"  void CheckerController_Start_m1291968488 (CheckerController_t1724959013 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CheckerController_Start_m1291968488_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral3921837499, 0, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral2281897856, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CheckerController::Update()
extern "C"  void CheckerController_Update_m3543050782 (CheckerController_t1724959013 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void CheckerController::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void CheckerController_OnTriggerEnter2D_m1874730116 (CheckerController_t1724959013 * __this, Collider2D_t2806799626 * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CheckerController_OnTriggerEnter2D_m1874730116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral2281897856, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0020;
		}
	}
	{
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral3921837499, 1, /*hidden argument*/NULL);
		goto IL_002b;
	}

IL_0020:
	{
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral2281897856, 1, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameController::.ctor()
extern "C"  void GameController__ctor_m1587060996 (GameController_t2330501625 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::Awake()
extern "C"  void GameController_Awake_m4256893697 (GameController_t2330501625 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_Awake_m4256893697_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral1512031223, 0, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral2002597352, _stringLiteral3454842817, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3964750823, _stringLiteral3454842817, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::Start()
extern "C"  void GameController_Start_m1173294274 (GameController_t2330501625 * __this, const RuntimeMethod* method)
{
	{
		Scene_t2348375561  L_0 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_scene_11(L_0);
		Scene_t2348375561 * L_1 = __this->get_address_of_scene_11();
		int32_t L_2 = Scene_get_buildIndex_m270272723(L_1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0034;
		}
	}
	{
		HeroController_t1470947597 * L_3 = __this->get_hero_5();
		NullCheck(L_3);
		HeroController_SetCameraStart_m2621664460(L_3, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_4 = __this->get_GameGUI_6();
		NullCheck(L_4);
		GameObject_SetActive_m796801857(L_4, (bool)1, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void GameController::Update()
extern "C"  void GameController_Update_m217025042 (GameController_t2330501625 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_Update_m217025042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		Text_t1901882714 * L_0 = __this->get_scoreText_2();
		int32_t L_1 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral1512031223, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = Int32_ToString_m141394615((&V_0), /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_2);
		HeroController_t1470947597 * L_3 = __this->get_hero_5();
		NullCheck(L_3);
		bool L_4 = HeroController_GetDeath_m1645202901(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_009b;
		}
	}
	{
		GameObject_t1113636619 * L_5 = __this->get_DeathGUI_9();
		NullCheck(L_5);
		GameObject_SetActive_m796801857(L_5, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_6 = __this->get_GameGUI_6();
		NullCheck(L_6);
		GameObject_SetActive_m796801857(L_6, (bool)0, /*hidden argument*/NULL);
		Text_t1901882714 * L_7 = __this->get_deathScore_3();
		int32_t L_8 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral1512031223, /*hidden argument*/NULL);
		V_1 = L_8;
		String_t* L_9 = Int32_ToString_m141394615((&V_1), /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_9);
		Text_t1901882714 * L_10 = __this->get_record_4();
		int32_t L_11 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3502151501, /*hidden argument*/NULL);
		V_2 = L_11;
		String_t* L_12 = Int32_ToString_m141394615((&V_2), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m3937257545(NULL /*static, unused*/, L_12, _stringLiteral3070507005, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_10, L_13);
	}

IL_009b:
	{
		return;
	}
}
// System.Void GameController::Play()
extern "C"  void GameController_Play_m3493558509 (GameController_t2330501625 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_Play_m3493558509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral2002597352, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, _stringLiteral3454842817, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		AudioSource_t3935305588 * L_2 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		NullCheck(L_2);
		AudioSource_Play_m48294159(L_2, /*hidden argument*/NULL);
	}

IL_0024:
	{
		GameObject_t1113636619 * L_3 = __this->get_GameGUI_6();
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_4 = __this->get_StartGUI_7();
		NullCheck(L_4);
		GameObject_SetActive_m796801857(L_4, (bool)0, /*hidden argument*/NULL);
		HeroController_t1470947597 * L_5 = __this->get_hero_5();
		NullCheck(L_5);
		HeroController_SetCameraStart_m2621664460(L_5, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::OpenSettings()
extern "C"  void GameController_OpenSettings_m1148998156 (GameController_t2330501625 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_OpenSettings_m1148998156_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral2002597352, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, _stringLiteral3454842817, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		AudioSource_t3935305588 * L_2 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		NullCheck(L_2);
		AudioSource_Play_m48294159(L_2, /*hidden argument*/NULL);
	}

IL_0024:
	{
		GameObject_t1113636619 * L_3 = __this->get_StartGUI_7();
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_4 = __this->get_SettingsGUI_8();
		NullCheck(L_4);
		GameObject_SetActive_m796801857(L_4, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::OpenStart()
extern "C"  void GameController_OpenStart_m1751541339 (GameController_t2330501625 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_OpenStart_m1751541339_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral2002597352, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, _stringLiteral3454842817, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		AudioSource_t3935305588 * L_2 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		NullCheck(L_2);
		AudioSource_Play_m48294159(L_2, /*hidden argument*/NULL);
	}

IL_0024:
	{
		GameObject_t1113636619 * L_3 = __this->get_SettingsGUI_8();
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_4 = __this->get_StartGUI_7();
		NullCheck(L_4);
		GameObject_SetActive_m796801857(L_4, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::RateApp()
extern "C"  void GameController_RateApp_m44342360 (GameController_t2330501625 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_RateApp_m44342360_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral2002597352, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, _stringLiteral3454842817, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		AudioSource_t3935305588 * L_2 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		NullCheck(L_2);
		AudioSource_Play_m48294159(L_2, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void GameController::Home()
extern "C"  void GameController_Home_m2940333881 (GameController_t2330501625 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_Home_m2940333881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral2002597352, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, _stringLiteral3454842817, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		AudioSource_t3935305588 * L_2 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		NullCheck(L_2);
		AudioSource_Play_m48294159(L_2, /*hidden argument*/NULL);
	}

IL_0024:
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::Restart()
extern "C"  void GameController_Restart_m3616958909 (GameController_t2330501625 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_Restart_m3616958909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral2002597352, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, _stringLiteral3454842817, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		AudioSource_t3935305588 * L_2 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		NullCheck(L_2);
		AudioSource_Play_m48294159(L_2, /*hidden argument*/NULL);
	}

IL_0024:
	{
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::Sound()
extern "C"  void GameController_Sound_m1311354126 (GameController_t2330501625 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_Sound_m1311354126_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral2002597352, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, _stringLiteral3454842817, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral2002597352, _stringLiteral133445101, /*hidden argument*/NULL);
		goto IL_003c;
	}

IL_002d:
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral2002597352, _stringLiteral3454842817, /*hidden argument*/NULL);
	}

IL_003c:
	{
		return;
	}
}
// System.Void GameController::Music()
extern "C"  void GameController_Music_m2239397093 (GameController_t2330501625 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_Music_m2239397093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3964750823, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, _stringLiteral3454842817, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003d;
		}
	}
	{
		GameObject_t1113636619 * L_2 = __this->get_mainCamera_10();
		NullCheck(L_2);
		AudioSource_t3935305588 * L_3 = GameObject_GetComponent_TisAudioSource_t3935305588_m625814604(L_2, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t3935305588_m625814604_RuntimeMethod_var);
		NullCheck(L_3);
		AudioSource_Stop_m2682712816(L_3, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3964750823, _stringLiteral133445101, /*hidden argument*/NULL);
		goto IL_005c;
	}

IL_003d:
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3964750823, _stringLiteral3454842817, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_4 = __this->get_mainCamera_10();
		NullCheck(L_4);
		AudioSource_t3935305588 * L_5 = GameObject_GetComponent_TisAudioSource_t3935305588_m625814604(L_4, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t3935305588_m625814604_RuntimeMethod_var);
		NullCheck(L_5);
		AudioSource_Play_m48294159(L_5, /*hidden argument*/NULL);
	}

IL_005c:
	{
		return;
	}
}
// System.Void GameController::RestorePurchases()
extern "C"  void GameController_RestorePurchases_m91018808 (GameController_t2330501625 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_RestorePurchases_m91018808_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral2002597352, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, _stringLiteral3454842817, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		AudioSource_t3935305588 * L_2 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		NullCheck(L_2);
		AudioSource_Play_m48294159(L_2, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void GameController::Leaderboard()
extern "C"  void GameController_Leaderboard_m3839236744 (GameController_t2330501625 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_Leaderboard_m3839236744_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral2002597352, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, _stringLiteral3454842817, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		AudioSource_t3935305588 * L_2 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		NullCheck(L_2);
		AudioSource_Play_m48294159(L_2, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void GameController::AD()
extern "C"  void GameController_AD_m3177971801 (GameController_t2330501625 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_AD_m3177971801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral2002597352, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, _stringLiteral3454842817, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		AudioSource_t3935305588 * L_2 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		NullCheck(L_2);
		AudioSource_Play_m48294159(L_2, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GateController::.ctor()
extern "C"  void GateController__ctor_m600729373 (GateController_t2541762910 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GateController__ctor_m600729373_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_sprites_4(((SpriteRendererU5BU5D_t911335936*)SZArrayNew(SpriteRendererU5BU5D_t911335936_il2cpp_TypeInfo_var, (uint32_t)4)));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GateController::Start()
extern "C"  void GateController_Start_m1837277945 (GateController_t2541762910 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GateController_Start_m1837277945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral1256528376, /*hidden argument*/NULL);
		__this->set_player_2(L_0);
		GameObject_t1113636619 * L_1 = __this->get_player_2();
		NullCheck(L_1);
		HeroController_t1470947597 * L_2 = GameObject_GetComponent_TisHeroController_t1470947597_m1641473813(L_1, /*hidden argument*/GameObject_GetComponent_TisHeroController_t1470947597_m1641473813_RuntimeMethod_var);
		__this->set_hero_3(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		SpriteRendererU5BU5D_t911335936* L_4 = GameObject_GetComponentsInChildren_TisSpriteRenderer_t3235626157_m3632004692(L_3, (bool)1, /*hidden argument*/GameObject_GetComponentsInChildren_TisSpriteRenderer_t3235626157_m3632004692_RuntimeMethod_var);
		__this->set_sprites_4(L_4);
		return;
	}
}
// System.Void GateController::Update()
extern "C"  void GateController_Update_m2083153653 (GateController_t2541762910 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void GateController::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void GateController_OnTriggerEnter2D_m2264937911 (GateController_t2541762910 * __this, Collider2D_t2806799626 * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GateController_OnTriggerEnter2D_m2264937911_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t2806799626 * L_0 = ___collision0;
		NullCheck(L_0);
		bool L_1 = Component_CompareTag_m1328479619(L_0, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00bc;
		}
	}
	{
		HeroController_t1470947597 * L_2 = __this->get_hero_3();
		NullCheck(L_2);
		int32_t L_3 = HeroController_GetShape_m2950644854(L_2, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_shapeGate_6();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_006d;
		}
	}
	{
		HeroController_t1470947597 * L_5 = __this->get_hero_3();
		NullCheck(L_5);
		int32_t L_6 = HeroController_GetColor_m1048228322(L_5, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_colorGate_5();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_006d;
		}
	}
	{
		HeroController_t1470947597 * L_8 = __this->get_hero_3();
		NullCheck(L_8);
		HeroController_IncreaseSpeed_m2256340442(L_8, /*hidden argument*/NULL);
		int32_t L_9 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral1512031223, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral1512031223, ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1)), /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_10 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		NullCheck(L_10);
		AudioSource_Play_m48294159(L_10, /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_006d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral1895510177, /*hidden argument*/NULL);
		int32_t L_11 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3502151501, /*hidden argument*/NULL);
		int32_t L_12 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral1512031223, /*hidden argument*/NULL);
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_00a4;
		}
	}
	{
		int32_t L_13 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral1512031223, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral3502151501, L_13, /*hidden argument*/NULL);
	}

IL_00a4:
	{
		HeroController_t1470947597 * L_14 = __this->get_hero_3();
		NullCheck(L_14);
		HeroController_SetMove_m3841425258(L_14, (bool)0, /*hidden argument*/NULL);
		HeroController_t1470947597 * L_15 = __this->get_hero_3();
		NullCheck(L_15);
		HeroController_SetDeath_m3098300077(L_15, (bool)1, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		return;
	}
}
// System.Void GateController::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void GateController_OnTriggerStay2D_m408863445 (GateController_t2541762910 * __this, Collider2D_t2806799626 * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GateController_OnTriggerStay2D_m408863445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t2806799626 * L_0 = ___collision0;
		NullCheck(L_0);
		bool L_1 = Component_CompareTag_m1328479619(L_0, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0072;
		}
	}
	{
		HeroController_t1470947597 * L_2 = __this->get_hero_3();
		NullCheck(L_2);
		bool L_3 = HeroController_GetMove_m3598639959(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0072;
		}
	}
	{
		HeroController_t1470947597 * L_4 = __this->get_hero_3();
		NullCheck(L_4);
		bool L_5 = HeroController_GetDeath_m1645202901(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3446927122, /*hidden argument*/NULL);
		SpriteRendererU5BU5D_t911335936* L_6 = __this->get_sprites_4();
		NullCheck(L_6);
		int32_t L_7 = 0;
		SpriteRenderer_t3235626157 * L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		Renderer_set_enabled_m1727253150(L_8, (bool)0, /*hidden argument*/NULL);
		SpriteRendererU5BU5D_t911335936* L_9 = __this->get_sprites_4();
		NullCheck(L_9);
		int32_t L_10 = 1;
		SpriteRenderer_t3235626157 * L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		Renderer_set_enabled_m1727253150(L_11, (bool)0, /*hidden argument*/NULL);
		SpriteRendererU5BU5D_t911335936* L_12 = __this->get_sprites_4();
		NullCheck(L_12);
		int32_t L_13 = 2;
		SpriteRenderer_t3235626157 * L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		Renderer_set_enabled_m1727253150(L_14, (bool)1, /*hidden argument*/NULL);
		SpriteRendererU5BU5D_t911335936* L_15 = __this->get_sprites_4();
		NullCheck(L_15);
		int32_t L_16 = 3;
		SpriteRenderer_t3235626157 * L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_17);
		Renderer_set_enabled_m1727253150(L_17, (bool)1, /*hidden argument*/NULL);
	}

IL_0072:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void HeroController::.ctor()
extern "C"  void HeroController__ctor_m2805344883 (HeroController_t1470947597 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HeroController__ctor_m2805344883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_speed_2((1.0f));
		il2cpp_array_size_t L_1[] = { (il2cpp_array_size_t)3, (il2cpp_array_size_t)3 };
		SpriteU5B0___U2C0___U5D_t2581906350* L_0 = (SpriteU5B0___U2C0___U5D_t2581906350*)GenArrayNew(SpriteU5B0___U2C0___U5D_t2581906350_il2cpp_TypeInfo_var, L_1);
		__this->set_sprites_22((SpriteU5B0___U2C0___U5D_t2581906350*)L_0);
		__this->set_color_23(2);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 HeroController::GetShape()
extern "C"  int32_t HeroController_GetShape_m2950644854 (HeroController_t1470947597 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_shape_24();
		return L_0;
	}
}
// System.Int32 HeroController::GetColor()
extern "C"  int32_t HeroController_GetColor_m1048228322 (HeroController_t1470947597 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_color_23();
		return L_0;
	}
}
// System.Boolean HeroController::GetMove()
extern "C"  bool HeroController_GetMove_m3598639959 (HeroController_t1470947597 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isMove_15();
		return L_0;
	}
}
// System.Void HeroController::SetMove(System.Boolean)
extern "C"  void HeroController_SetMove_m3841425258 (HeroController_t1470947597 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_isMove_15(L_0);
		return;
	}
}
// System.Boolean HeroController::GetCameraStart()
extern "C"  bool HeroController_GetCameraStart_m2480587098 (HeroController_t1470947597 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_cameraStart_16();
		return L_0;
	}
}
// System.Void HeroController::SetCameraStart(System.Boolean)
extern "C"  void HeroController_SetCameraStart_m2621664460 (HeroController_t1470947597 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_cameraStart_16(L_0);
		return;
	}
}
// System.Boolean HeroController::GetDeath()
extern "C"  bool HeroController_GetDeath_m1645202901 (HeroController_t1470947597 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isDeath_17();
		return L_0;
	}
}
// System.Void HeroController::SetDeath(System.Boolean)
extern "C"  void HeroController_SetDeath_m3098300077 (HeroController_t1470947597 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_isDeath_17(L_0);
		return;
	}
}
// System.Boolean HeroController::GetIsJump()
extern "C"  bool HeroController_GetIsJump_m1085680721 (HeroController_t1470947597 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isJump_19();
		return L_0;
	}
}
// System.Void HeroController::SetIsJump(System.Boolean)
extern "C"  void HeroController_SetIsJump_m358591439 (HeroController_t1470947597 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_isJump_19(L_0);
		return;
	}
}
// System.Void HeroController::IncreaseSpeed()
extern "C"  void HeroController_IncreaseSpeed_m2256340442 (HeroController_t1470947597 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_speed_2();
		float L_1 = __this->get_increaseSpeed_5();
		__this->set_speed_2(((float)il2cpp_codegen_add((float)L_0, (float)L_1)));
		return;
	}
}
// System.Void HeroController::Start()
extern "C"  void HeroController_Start_m163809011 (HeroController_t1470947597 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HeroController_Start_m163809011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpriteU5B0___U2C0___U5D_t2581906350* L_0 = __this->get_sprites_22();
		Sprite_t280657092 * L_1 = __this->get_yelCirc_6();
		NullCheck((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_0);
		((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_0)->SetAt(0, 0, L_1);
		SpriteU5B0___U2C0___U5D_t2581906350* L_2 = __this->get_sprites_22();
		Sprite_t280657092 * L_3 = __this->get_yelClock_7();
		NullCheck((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_2);
		((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_2)->SetAt(0, 1, L_3);
		SpriteU5B0___U2C0___U5D_t2581906350* L_4 = __this->get_sprites_22();
		Sprite_t280657092 * L_5 = __this->get_yelRhomb_8();
		NullCheck((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_4);
		((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_4)->SetAt(0, 2, L_5);
		SpriteU5B0___U2C0___U5D_t2581906350* L_6 = __this->get_sprites_22();
		Sprite_t280657092 * L_7 = __this->get_redCirc_9();
		NullCheck((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_6);
		((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_6)->SetAt(1, 0, L_7);
		SpriteU5B0___U2C0___U5D_t2581906350* L_8 = __this->get_sprites_22();
		Sprite_t280657092 * L_9 = __this->get_redClock_10();
		NullCheck((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_8);
		((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_8)->SetAt(1, 1, L_9);
		SpriteU5B0___U2C0___U5D_t2581906350* L_10 = __this->get_sprites_22();
		Sprite_t280657092 * L_11 = __this->get_redRhomb_11();
		NullCheck((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_10);
		((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_10)->SetAt(1, 2, L_11);
		SpriteU5B0___U2C0___U5D_t2581906350* L_12 = __this->get_sprites_22();
		Sprite_t280657092 * L_13 = __this->get_violetCirc_12();
		NullCheck((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_12);
		((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_12)->SetAt(2, 0, L_13);
		SpriteU5B0___U2C0___U5D_t2581906350* L_14 = __this->get_sprites_22();
		Sprite_t280657092 * L_15 = __this->get_violetClock_13();
		NullCheck((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_14);
		((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_14)->SetAt(2, 1, L_15);
		SpriteU5B0___U2C0___U5D_t2581906350* L_16 = __this->get_sprites_22();
		Sprite_t280657092 * L_17 = __this->get_violetRhomb_14();
		NullCheck((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_16);
		((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_16)->SetAt(2, 2, L_17);
		SpriteRenderer_t3235626157 * L_18 = Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502_RuntimeMethod_var);
		__this->set_heroSprite_20(L_18);
		Rigidbody2D_t939494601 * L_19 = Component_GetComponent_TisRigidbody2D_t939494601_m1531613439(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var);
		__this->set_rb_21(L_19);
		return;
	}
}
// System.Void HeroController::FixedUpdate()
extern "C"  void HeroController_FixedUpdate_m1083043067 (HeroController_t1470947597 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HeroController_FixedUpdate_m1083043067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		HeroController_CheckGround_m2788034350(__this, /*hidden argument*/NULL);
		Rigidbody2D_t939494601 * L_0 = __this->get_rb_21();
		NullCheck(L_0);
		Vector2_t2156229523  L_1 = Rigidbody2D_get_velocity_m366589732(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = Vector2_get_magnitude_m2752892833((&V_0), /*hidden argument*/NULL);
		float L_3 = __this->get_maxSpeed_4();
		if ((!(((float)L_2) > ((float)L_3))))
		{
			goto IL_004d;
		}
	}
	{
		Rigidbody2D_t939494601 * L_4 = __this->get_rb_21();
		Rigidbody2D_t939494601 * L_5 = __this->get_rb_21();
		NullCheck(L_5);
		Vector2_t2156229523  L_6 = Rigidbody2D_get_velocity_m366589732(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Vector2_t2156229523  L_7 = Vector2_get_normalized_m2683665860((&V_1), /*hidden argument*/NULL);
		float L_8 = __this->get_maxSpeed_4();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_9 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_4);
		Rigidbody2D_set_velocity_m2898400508(L_4, L_9, /*hidden argument*/NULL);
	}

IL_004d:
	{
		return;
	}
}
// System.Void HeroController::Update()
extern "C"  void HeroController_Update_m2266239483 (HeroController_t1470947597 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HeroController_Update_m2266239483_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_isMove_15();
		if (!L_0)
		{
			goto IL_006d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		int32_t L_1 = Input_get_touchCount_m3403849067(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0032;
		}
	}
	{
		bool L_2 = __this->get_isGrounded_18();
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		bool L_3 = __this->get_isJump_19();
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		HeroController_Swipe_m2874113322(__this, /*hidden argument*/NULL);
	}

IL_0032:
	{
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t3722313464  L_6 = Transform_get_position_m36019626(L_5, /*hidden argument*/NULL);
		Transform_t3600365921 * L_7 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t3722313464  L_8 = Transform_get_right_m2535262102(L_7, /*hidden argument*/NULL);
		float L_9 = __this->get_speed_2();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_10 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		float L_11 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		Vector3_t3722313464  L_13 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_6, L_12, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_position_m3387557959(L_4, L_13, /*hidden argument*/NULL);
	}

IL_006d:
	{
		return;
	}
}
// System.Void HeroController::Swipe()
extern "C"  void HeroController_Swipe_m2874113322 (HeroController_t1470947597 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HeroController_Swipe_m2874113322_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Touch_t1921856868  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_0 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_0;
		Vector2_t2156229523  L_1 = Touch_get_deltaPosition_m2389653382((&V_1), /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_y_1();
		if ((!(((float)L_2) > ((float)(1.0f)))))
		{
			goto IL_0058;
		}
	}
	{
		Rigidbody2D_t939494601 * L_3 = __this->get_rb_21();
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t3722313464  L_5 = Transform_get_up_m3972993886(L_4, /*hidden argument*/NULL);
		float L_6 = (&V_0)->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Sign_m3457838305(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_8 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/NULL);
		float L_9 = __this->get_jumpForce_3();
		Vector3_t3722313464  L_10 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_11 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_3);
		Rigidbody2D_AddForce_m1099013366(L_3, L_11, 1, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void HeroController::Move()
extern "C"  void HeroController_Move_m2322399700 (HeroController_t1470947597 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HeroController_Move_m2322399700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody2D_t939494601 * L_0 = __this->get_rb_21();
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3722313464  L_2 = Transform_get_up_m3972993886(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_3 = Input_GetAxis_m4009438427(NULL /*static, unused*/, _stringLiteral2984908384, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		float L_5 = __this->get_jumpForce_3();
		Vector3_t3722313464  L_6 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_7 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rigidbody2D_AddForce_m1099013366(L_0, L_7, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HeroController::colorClick()
extern "C"  void HeroController_colorClick_m115231268 (HeroController_t1470947597 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_color_23();
		__this->set_color_23(((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1)));
		int32_t L_1 = __this->get_color_23();
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0021;
		}
	}
	{
		__this->set_color_23(0);
	}

IL_0021:
	{
		SpriteRenderer_t3235626157 * L_2 = __this->get_heroSprite_20();
		SpriteU5B0___U2C0___U5D_t2581906350* L_3 = __this->get_sprites_22();
		int32_t L_4 = __this->get_color_23();
		int32_t L_5 = __this->get_shape_24();
		NullCheck((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_3);
		Sprite_t280657092 * L_6 = ((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_3)->GetAt(L_4, L_5);
		NullCheck(L_2);
		SpriteRenderer_set_sprite_m1286893786(L_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HeroController::shapeClick()
extern "C"  void HeroController_shapeClick_m2302741635 (HeroController_t1470947597 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_shape_24();
		__this->set_shape_24(((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1)));
		int32_t L_1 = __this->get_shape_24();
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0021;
		}
	}
	{
		__this->set_shape_24(0);
	}

IL_0021:
	{
		SpriteRenderer_t3235626157 * L_2 = __this->get_heroSprite_20();
		SpriteU5B0___U2C0___U5D_t2581906350* L_3 = __this->get_sprites_22();
		int32_t L_4 = __this->get_color_23();
		int32_t L_5 = __this->get_shape_24();
		NullCheck((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_3);
		Sprite_t280657092 * L_6 = ((SpriteU5B0___U2C0___U5D_t2581906350*)(SpriteU5B0___U2C0___U5D_t2581906350*)L_3)->GetAt(L_4, L_5);
		NullCheck(L_2);
		SpriteRenderer_set_sprite_m1286893786(L_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HeroController::CheckGround()
extern "C"  void HeroController_CheckGround_m2788034350 (HeroController_t1470947597 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HeroController_CheckGround_m2788034350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Collider2DU5BU5D_t1693969295* V_0 = NULL;
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_2 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1693969295* L_3 = Physics2D_OverlapCircleAll_m841024216(NULL /*static, unused*/, L_2, (0.8f), /*hidden argument*/NULL);
		V_0 = L_3;
		Collider2DU5BU5D_t1693969295* L_4 = V_0;
		NullCheck(L_4);
		__this->set_isGrounded_18((bool)((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_4)->max_length))))) > ((int32_t)1))? 1 : 0));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JumpZone::.ctor()
extern "C"  void JumpZone__ctor_m780936948 (JumpZone_t2032044937 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JumpZone::Start()
extern "C"  void JumpZone_Start_m3110681720 (JumpZone_t2032044937 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JumpZone_Start_m3110681720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral1256528376, /*hidden argument*/NULL);
		__this->set_player_2(L_0);
		GameObject_t1113636619 * L_1 = __this->get_player_2();
		NullCheck(L_1);
		HeroController_t1470947597 * L_2 = GameObject_GetComponent_TisHeroController_t1470947597_m1641473813(L_1, /*hidden argument*/GameObject_GetComponent_TisHeroController_t1470947597_m1641473813_RuntimeMethod_var);
		__this->set_hero_3(L_2);
		return;
	}
}
// System.Void JumpZone::Update()
extern "C"  void JumpZone_Update_m2556213886 (JumpZone_t2032044937 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void JumpZone::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void JumpZone_OnTriggerEnter2D_m2019572104 (JumpZone_t2032044937 * __this, Collider2D_t2806799626 * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JumpZone_OnTriggerEnter2D_m2019572104_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t2806799626 * L_0 = ___collision0;
		NullCheck(L_0);
		bool L_1 = Component_CompareTag_m1328479619(L_0, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		HeroController_t1470947597 * L_2 = __this->get_hero_3();
		NullCheck(L_2);
		HeroController_SetIsJump_m358591439(L_2, (bool)1, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void JumpZone::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void JumpZone_OnTriggerExit2D_m373128292 (JumpZone_t2032044937 * __this, Collider2D_t2806799626 * ___collision0, const RuntimeMethod* method)
{
	{
		HeroController_t1470947597 * L_0 = __this->get_hero_3();
		NullCheck(L_0);
		HeroController_SetIsJump_m358591439(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LevelController::.ctor()
extern "C"  void LevelController__ctor_m3999760241 (LevelController_t24418946 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelController__ctor_m3999760241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_countPlatform_4((-1));
		__this->set_platforms_6(((GameObjectU5BU5D_t3328599146*)SZArrayNew(GameObjectU5BU5D_t3328599146_il2cpp_TypeInfo_var, (uint32_t)2)));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LevelController::Awake()
extern "C"  void LevelController_Awake_m2998741377 (LevelController_t24418946 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelController_Awake_m2998741377_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Resources_Load_TisGameObject_t1113636619_m1734345100(NULL /*static, unused*/, _stringLiteral38631985, /*hidden argument*/Resources_Load_TisGameObject_t1113636619_m1734345100_RuntimeMethod_var);
		__this->set_platformPrefab_2(L_0);
		Vector3_t3722313464  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m3353183577((&L_1), (23.9f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_startPosition_3(L_1);
		return;
	}
}
// System.Void LevelController::Start()
extern "C"  void LevelController_Start_m3354251513 (LevelController_t24418946 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelController_Start_m3354251513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObjectU5BU5D_t3328599146* L_0 = __this->get_platforms_6();
		GameObject_t1113636619 * L_1 = __this->get_platformPrefab_2();
		Vector3_t3722313464  L_2 = __this->get_startPosition_3();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_3 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_4 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_4);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (GameObject_t1113636619 *)L_4);
		Vector3_t3722313464 * L_5 = __this->get_address_of_startPosition_3();
		Vector3_t3722313464 * L_6 = L_5;
		float L_7 = L_6->get_x_1();
		L_6->set_x_1(((float)il2cpp_codegen_add((float)L_7, (float)(32.35f))));
		Vector3_t3722313464 * L_8 = __this->get_address_of_startPosition_3();
		L_8->set_y_2((1.2f));
		GameObjectU5BU5D_t3328599146* L_9 = __this->get_platforms_6();
		GameObject_t1113636619 * L_10 = __this->get_platformPrefab_2();
		Vector3_t3722313464  L_11 = __this->get_startPosition_3();
		Quaternion_t2301928331  L_12 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_13 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_10, L_11, L_12, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_13);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(1), (GameObject_t1113636619 *)L_13);
		Vector3_t3722313464 * L_14 = __this->get_address_of_startPosition_3();
		Vector3_t3722313464 * L_15 = L_14;
		float L_16 = L_15->get_x_1();
		L_15->set_x_1(((float)il2cpp_codegen_add((float)L_16, (float)(32.35f))));
		int32_t L_17 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral1512031223, /*hidden argument*/NULL);
		__this->set_localScore_7(L_17);
		return;
	}
}
// System.Void LevelController::Update()
extern "C"  void LevelController_Update_m1452339333 (LevelController_t24418946 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelController_Update_m1452339333_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral1512031223, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_localScore_7();
		if ((!(((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)L_1))) == ((uint32_t)5))))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_2 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral1512031223, /*hidden argument*/NULL);
		__this->set_localScore_7(L_2);
		V_0 = (1.0f);
		int32_t L_3 = __this->get_changePos_5();
		__this->set_changePos_5(((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1)));
		int32_t L_4 = __this->get_changePos_5();
		if ((!(((uint32_t)L_4) == ((uint32_t)3))))
		{
			goto IL_004e;
		}
	}
	{
		__this->set_changePos_5(1);
	}

IL_004e:
	{
		int32_t L_5 = __this->get_changePos_5();
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_0065;
		}
	}
	{
		V_0 = (1.2f);
		goto IL_0077;
	}

IL_0065:
	{
		int32_t L_6 = __this->get_changePos_5();
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_0077;
		}
	}
	{
		V_0 = (0.0f);
	}

IL_0077:
	{
		Vector3_t3722313464 * L_7 = __this->get_address_of_startPosition_3();
		float L_8 = V_0;
		L_7->set_y_2(L_8);
	}

IL_0083:
	{
		int32_t L_9 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3921837499, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_0121;
		}
	}
	{
		int32_t L_10 = __this->get_countPlatform_4();
		__this->set_countPlatform_4(((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1)));
		int32_t L_11 = __this->get_countPlatform_4();
		if ((!(((uint32_t)L_11) == ((uint32_t)2))))
		{
			goto IL_00b4;
		}
	}
	{
		__this->set_countPlatform_4(0);
	}

IL_00b4:
	{
		GameObjectU5BU5D_t3328599146* L_12 = __this->get_platforms_6();
		int32_t L_13 = __this->get_countPlatform_4();
		NullCheck(L_12);
		int32_t L_14 = L_13;
		GameObject_t1113636619 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_15);
		PlatformController_t389759191 * L_16 = GameObject_GetComponent_TisPlatformController_t389759191_m3531744381(L_15, /*hidden argument*/GameObject_GetComponent_TisPlatformController_t389759191_m3531744381_RuntimeMethod_var);
		NullCheck(L_16);
		PlatformController_DeleteGates_m2200208598(L_16, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_17 = __this->get_platforms_6();
		int32_t L_18 = __this->get_countPlatform_4();
		NullCheck(L_17);
		int32_t L_19 = L_18;
		GameObject_t1113636619 * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck(L_20);
		PlatformController_t389759191 * L_21 = GameObject_GetComponent_TisPlatformController_t389759191_m3531744381(L_20, /*hidden argument*/GameObject_GetComponent_TisPlatformController_t389759191_m3531744381_RuntimeMethod_var);
		NullCheck(L_21);
		PlatformController_CreateGates_m1101393362(L_21, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_22 = __this->get_platforms_6();
		int32_t L_23 = __this->get_countPlatform_4();
		NullCheck(L_22);
		int32_t L_24 = L_23;
		GameObject_t1113636619 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		NullCheck(L_25);
		Transform_t3600365921 * L_26 = GameObject_get_transform_m1369836730(L_25, /*hidden argument*/NULL);
		Vector3_t3722313464  L_27 = __this->get_startPosition_3();
		NullCheck(L_26);
		Transform_set_position_m3387557959(L_26, L_27, /*hidden argument*/NULL);
		Vector3_t3722313464 * L_28 = __this->get_address_of_startPosition_3();
		Vector3_t3722313464 * L_29 = L_28;
		float L_30 = L_29->get_x_1();
		L_29->set_x_1(((float)il2cpp_codegen_add((float)L_30, (float)(32.35f))));
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral3921837499, 0, /*hidden argument*/NULL);
	}

IL_0121:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlatformColliderController::.ctor()
extern "C"  void PlatformColliderController__ctor_m2539429813 (PlatformColliderController_t1042155798 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlatformColliderController::Start()
extern "C"  void PlatformColliderController_Start_m1491561211 (PlatformColliderController_t1042155798 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformColliderController_Start_m1491561211_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral1256528376, /*hidden argument*/NULL);
		__this->set_player_2(L_0);
		GameObject_t1113636619 * L_1 = __this->get_player_2();
		NullCheck(L_1);
		HeroController_t1470947597 * L_2 = GameObject_GetComponent_TisHeroController_t1470947597_m1641473813(L_1, /*hidden argument*/GameObject_GetComponent_TisHeroController_t1470947597_m1641473813_RuntimeMethod_var);
		__this->set_hero_3(L_2);
		return;
	}
}
// System.Void PlatformColliderController::Update()
extern "C"  void PlatformColliderController_Update_m2108854829 (PlatformColliderController_t1042155798 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void PlatformColliderController::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void PlatformColliderController_OnTriggerEnter2D_m3155749451 (PlatformColliderController_t1042155798 * __this, Collider2D_t2806799626 * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformColliderController_OnTriggerEnter2D_m3155749451_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t2806799626 * L_0 = ___collision0;
		NullCheck(L_0);
		bool L_1 = Component_CompareTag_m1328479619(L_0, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_2 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3502151501, /*hidden argument*/NULL);
		int32_t L_3 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral1512031223, /*hidden argument*/NULL);
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral1512031223, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral3502151501, L_4, /*hidden argument*/NULL);
	}

IL_003d:
	{
		HeroController_t1470947597 * L_5 = __this->get_hero_3();
		NullCheck(L_5);
		HeroController_SetMove_m3841425258(L_5, (bool)0, /*hidden argument*/NULL);
		HeroController_t1470947597 * L_6 = __this->get_hero_3();
		NullCheck(L_6);
		HeroController_SetDeath_m3098300077(L_6, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral4069454954, /*hidden argument*/NULL);
	}

IL_005f:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlatformController::.ctor()
extern "C"  void PlatformController__ctor_m143673660 (PlatformController_t389759191 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlatformController::Awake()
extern "C"  void PlatformController_Awake_m672361463 (PlatformController_t389759191 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformController_Awake_m672361463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObjectU5BU5D_t3328599146* L_0 = Resources_LoadAll_TisGameObject_t1113636619_m2490946171(NULL /*static, unused*/, _stringLiteral78692068, /*hidden argument*/Resources_LoadAll_TisGameObject_t1113636619_m2490946171_RuntimeMethod_var);
		__this->set_gates_2(L_0);
		GameObject_t1113636619 * L_1 = Resources_Load_TisGameObject_t1113636619_m1734345100(NULL /*static, unused*/, _stringLiteral3128803744, /*hidden argument*/Resources_Load_TisGameObject_t1113636619_m1734345100_RuntimeMethod_var);
		__this->set_groundPrefab_4(L_1);
		__this->set_instGates_3(((GameObjectU5BU5D_t3328599146*)SZArrayNew(GameObjectU5BU5D_t3328599146_il2cpp_TypeInfo_var, (uint32_t)5)));
		__this->set_positions_6(((Vector3U5BU5D_t1718750761*)SZArrayNew(Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var, (uint32_t)5)));
		Vector3U5BU5D_t1718750761* L_2 = __this->get_positions_6();
		NullCheck(L_2);
		Vector3_t3722313464  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m3353183577((&L_3), (-16.52f), (0.649f), (0.0f), /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_3;
		Vector3U5BU5D_t1718750761* L_4 = __this->get_positions_6();
		NullCheck(L_4);
		Vector3_t3722313464  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector3__ctor_m3353183577((&L_5), (-10.52f), (0.649f), (0.0f), /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_5;
		Vector3U5BU5D_t1718750761* L_6 = __this->get_positions_6();
		NullCheck(L_6);
		Vector3_t3722313464  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m3353183577((&L_7), (-4.52f), (0.649f), (0.0f), /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_7;
		Vector3U5BU5D_t1718750761* L_8 = __this->get_positions_6();
		NullCheck(L_8);
		Vector3_t3722313464  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m3353183577((&L_9), (1.48f), (0.649f), (0.0f), /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))) = L_9;
		Vector3U5BU5D_t1718750761* L_10 = __this->get_positions_6();
		NullCheck(L_10);
		Vector3_t3722313464  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector3__ctor_m3353183577((&L_11), (7.48f), (0.649f), (0.0f), /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(4))) = L_11;
		Vector3_t3722313464  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m3353183577((&L_12), (-2.8f), (-0.55f), (0.0f), /*hidden argument*/NULL);
		__this->set_positionGround_7(L_12);
		return;
	}
}
// System.Void PlatformController::Start()
extern "C"  void PlatformController_Start_m1630760214 (PlatformController_t389759191 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformController_Start_m1630760214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = __this->get_groundPrefab_4();
		Vector3_t3722313464  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m3353183577((&L_1), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_2 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_3 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		__this->set_ground_5(L_3);
		GameObject_t1113636619 * L_4 = __this->get_ground_5();
		NullCheck(L_4);
		Transform_t3600365921 * L_5 = GameObject_get_transform_m1369836730(L_4, /*hidden argument*/NULL);
		Transform_t3600365921 * L_6 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_SetParent_m381167889(L_5, L_6, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_7 = __this->get_ground_5();
		NullCheck(L_7);
		Transform_t3600365921 * L_8 = GameObject_get_transform_m1369836730(L_7, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = __this->get_positionGround_7();
		NullCheck(L_8);
		Transform_set_localPosition_m4128471975(L_8, L_9, /*hidden argument*/NULL);
		PlatformController_CreateGates_m1101393362(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlatformController::Update()
extern "C"  void PlatformController_Update_m560157830 (PlatformController_t389759191 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void PlatformController::DeleteGates()
extern "C"  void PlatformController_DeleteGates_m2200208598 (PlatformController_t389759191 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformController_DeleteGates_m2200208598_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0018;
	}

IL_0007:
	{
		GameObjectU5BU5D_t3328599146* L_0 = __this->get_instGates_3();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_t1113636619 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0018:
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)5)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PlatformController::CreateGates()
extern "C"  void PlatformController_CreateGates_m1101393362 (PlatformController_t389759191 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformController_CreateGates_m1101393362_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = (-1);
		V_1 = 0;
		V_2 = 0;
		goto IL_0090;
	}

IL_000b:
	{
		GameObjectU5BU5D_t3328599146* L_0 = __this->get_gates_2();
		NullCheck(L_0);
		int32_t L_1 = Random_Range_m4054026115(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length)))), /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_000b;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_4 = __this->get_instGates_3();
		int32_t L_5 = V_2;
		GameObjectU5BU5D_t3328599146* L_6 = __this->get_gates_2();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		GameObject_t1113636619 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		Vector3_t3722313464  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m3353183577((&L_10), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_11 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_12 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_12);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (GameObject_t1113636619 *)L_12);
		GameObjectU5BU5D_t3328599146* L_13 = __this->get_instGates_3();
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		GameObject_t1113636619 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		Transform_t3600365921 * L_17 = GameObject_get_transform_m1369836730(L_16, /*hidden argument*/NULL);
		Transform_t3600365921 * L_18 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_SetParent_m381167889(L_17, L_18, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_19 = __this->get_instGates_3();
		int32_t L_20 = V_2;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		GameObject_t1113636619 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		Transform_t3600365921 * L_23 = GameObject_get_transform_m1369836730(L_22, /*hidden argument*/NULL);
		Vector3U5BU5D_t1718750761* L_24 = __this->get_positions_6();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		NullCheck(L_23);
		Transform_set_localPosition_m4128471975(L_23, (*(Vector3_t3722313464 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_25)))), /*hidden argument*/NULL);
		int32_t L_26 = V_1;
		V_0 = L_26;
		int32_t L_27 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_27, (int32_t)1));
	}

IL_0090:
	{
		int32_t L_28 = V_2;
		if ((((int32_t)L_28) < ((int32_t)5)))
		{
			goto IL_000b;
		}
	}
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
