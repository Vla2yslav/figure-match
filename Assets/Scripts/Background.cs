﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Background : MonoBehaviour
{
    #region Fields

    private Image _backgroundImage;
    private Sprite[] _backgroundSprites;

    #endregion

    #region Main Methods

    private void Awake()
    {
        _backgroundImage = GetComponent<Image>();

        _backgroundSprites = Resources.LoadAll("Backgrounds", typeof(Sprite)).Cast<Sprite>().ToArray();
        _backgroundImage.sprite = _backgroundSprites[Random.Range(0, _backgroundSprites.Length)];
    }

    #endregion
}
