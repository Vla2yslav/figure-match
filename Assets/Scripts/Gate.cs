﻿using UnityEngine;

public class Gate : MonoBehaviour
{
    #region Fields

    [Header("Main settings")]
    [SerializeField] private Preferences _preferences;
    private SpriteRenderer[] _spriteRenderers;
    private AudioSource _audio;
    private Color _color;
    private Player _player;

    private int _colorIndex;
    private int _figureIndex;

    public int ColorIndex => _colorIndex;
    public int FigureIndex => _figureIndex;

    #endregion

    #region Main Methods

    private void Awake()
    {
        _spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
        _audio = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (_player == null)
            _player = collision.GetComponent<Player>();
        
        CheckMatch();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (_player == null)
            _player = collision.GetComponent<Player>();
        
        if (_player.Move)
            DeactivateGate();
    }

    #endregion

    #region Helper Methods

    public void InitializeGate()
    {
        RandomizeColor();
        RandomizeFigure();

        SetColor(_preferences.Colors[_colorIndex]);
        SetFigure(_preferences.Gates[_figureIndex]);
    }

    private void RandomizeColor()
    {
        _colorIndex = Random.Range(0, _preferences.Colors.Count);
    }

    private void RandomizeFigure()
    {
        _figureIndex = Random.Range(0, _preferences.Gates.Count);
    }

    private void DeactivateGate()
    {
        SetColor(_preferences.DeactivateColor);
    }

    private void SetColor(Color color)
    {
        foreach (SpriteRenderer spriteRenderer in _spriteRenderers)
            spriteRenderer.color = color;
    }

    private void SetFigure(Gate_Template gate)
    {
        for (int i = 0; i < _spriteRenderers.Length; i++)
            _spriteRenderers[i].sprite = gate.Sprites[i];
    }

    private void CheckMatch()
    {
        if (_player.Figure.CurrentFigureIndex == _figureIndex &&
                 _player.Figure.CurrentColorIndex == _colorIndex)
        {
            _player.CorrectPassage();
            _audio.Play();
        }
        else
            _player.IncorrectPassage();
    }

    #endregion
}
