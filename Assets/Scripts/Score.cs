﻿using UnityEngine;
using TMPro;

public class Score : MonoBehaviour
{
    #region Fields

    [Header("Main settings")]
    private TextMeshProUGUI _scoreText;
    private int _scoreAmount = 0;
    public int ScoreAmount => _scoreAmount;

    #endregion

    #region Main Methods

    private void Awake()
    {
        _scoreText = GetComponent<TextMeshProUGUI>();
        UpdateScore();
    }

    #endregion

    #region Helper Methods

    public void UpdateScore()
    {
        _scoreText.text = _scoreAmount.ToString();
    }

    public void IncreaseScore()
    {
        _scoreAmount++;
        UpdateScore();
    }

    public void SaveScore()
    {
        PlayerPrefs.SetInt("score", _scoreAmount);

        if (PlayerPrefs.HasKey("record"))
        {
            if(PlayerPrefs.GetInt("record") < _scoreAmount)
                PlayerPrefs.SetInt("record", _scoreAmount);
        }
        else
            PlayerPrefs.SetInt("record", _scoreAmount);
    }
    
    #endregion
}
