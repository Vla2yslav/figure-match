﻿using UnityEngine;
using UnityEngine.Events;

namespace VV.UI
{
    [RequireComponent(typeof(CanvasGroup))]
    [RequireComponent(typeof(Animator))]
    public class UI_Screen : MonoBehaviour
    {
        #region Fields
        
        [Header("System Event")]
        public UnityEvent onStartScreen = new UnityEvent();
        public UnityEvent onCloseScreen = new UnityEvent();

        private Animator _animator;

        #endregion

        #region Main Methods

        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }
        
        #endregion

        #region Helper Methods

        public virtual void StartScreen()
        {
            if (onStartScreen != null)
                onStartScreen.Invoke();
            
            HandleTrigger("show");
        }

        public virtual void CloseScreen()
        {
            if (onCloseScreen != null)
                onCloseScreen.Invoke();

            HandleTrigger("hide");
        }

        public void DeactivateScreen()
        {
            gameObject.SetActive(false);
        }

        void HandleTrigger(string trigger)
        {
            if (_animator)
                _animator.SetTrigger(trigger);
        }

        #endregion
    }
}
