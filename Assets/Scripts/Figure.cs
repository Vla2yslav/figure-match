﻿using UnityEngine;

public class Figure : MonoBehaviour
{
    #region Fields

    [Header("Main settings")]
    [SerializeField] private Preferences _preferences;
    [SerializeField] private float _speedChangeColor;
    private SpriteRenderer _spriteRenderer;


    private int _currentFigureIndex = 0, _currentColorIndex = 0;
    private bool _isChangingColor = false;
    public int CurrentFigureIndex => _currentFigureIndex;
    public int CurrentColorIndex => _currentColorIndex;

    #endregion

    #region Main Methods

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        
        _spriteRenderer.sprite = _preferences.Figures[_currentFigureIndex].sprite;
        _spriteRenderer.color = _preferences.Colors[_currentColorIndex];
    }

    private void Update()
    {
        if (_isChangingColor)
            ChangeGradientColor();
    }

    #endregion

    #region Helper Methods

    public void SwitchFigure()
    {
        _currentFigureIndex++;

        if (_currentFigureIndex == _preferences.Figures.Count)
            _currentFigureIndex = 0;

        UpdateSprite();
    }

    public void SwitchColor()
    {
        _currentColorIndex++;

        if (_currentColorIndex == _preferences.Colors.Count)
            _currentColorIndex = 0;

        _isChangingColor = true;
    }

    private void UpdateSprite()
    {
        _spriteRenderer.sprite = _preferences.Figures[_currentFigureIndex].sprite;
        _spriteRenderer.color = _preferences.Colors[_currentColorIndex];
    }

    private void ChangeGradientColor()
    {
        _spriteRenderer.color = Color.Lerp(_spriteRenderer.color, _preferences.Colors[_currentColorIndex], 
                                           _speedChangeColor * Time.deltaTime);

        if (_spriteRenderer.color == _preferences.Colors[_currentColorIndex])
            _isChangingColor = false;
    }

    #endregion
}
