﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Figure", menuName = "FM/Figure")]
public class Figure_Template : ScriptableObject
{
    public string Name;
    public Sprite sprite;
}
