﻿using UnityEngine;
using UnityEngine.Events;

namespace VV.UI
{
    public class UI_System : MonoBehaviour
    {
        #region Fields

        [Header("Main settings")]
        [SerializeField] private UI_Screen _startScreen;
        public UnityEvent onSwitchedScreen = new UnityEvent();

        private Component[] _screens = new Component[0];
        private UI_Screen _currentScreen;
        private UI_Screen _previousScreen;

        public UI_Screen CurrentScreen => _currentScreen;
        public UI_Screen PreviousScreen => _previousScreen;

        #endregion

        #region Main Methods

        private void Start()
        {
            _screens = GetComponentsInChildren<UI_Screen>(true);

            if (_startScreen)
                SwithScreen(_startScreen);
        }

        #endregion

        #region Helper Methods

        public void SwithScreen(UI_Screen screen)
        {
            if (screen)
            {
                if (_currentScreen)
                {
                    _currentScreen.CloseScreen();
                    _previousScreen = _currentScreen;
                }

                _currentScreen = screen;
                _currentScreen.gameObject.SetActive(true);
                _currentScreen.StartScreen();

                if (onSwitchedScreen != null)
                    onSwitchedScreen.Invoke();
            }
        }

        public void SwitchToPreviousScreen()
        {
            if (_previousScreen)
                SwithScreen(_previousScreen);
        }

        #endregion
    }
}