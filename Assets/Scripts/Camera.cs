﻿using UnityEngine;

public class Camera : MonoBehaviour
{
    #region Fields

    [Header("Main settings")]
    [SerializeField] private Player _player;
    [SerializeField] [Range(0, 10)] private float _space;

    #endregion

    #region Main Methods

    private void Update()
    {
        if (_player.Move)
        {
            Vector3 position = transform.position;
            position.x = _player.transform.position.x + _space;
            transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime);
        }
    }

    #endregion
}
