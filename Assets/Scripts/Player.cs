﻿using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    #region Fields

    [Header("Main settings")]

    [SerializeField] [Range(0, 10)] private float _speed;
    [SerializeField] [Range(0, 10)] private float _maxSpeed;
    [SerializeField] [Range(0, 10)] private float _increaseSpeed;
    [SerializeField] [Range(0, 10)] private float _jumpForce;
    [SerializeField] private bool _move;

    public bool Move { get {return _move;} set {_move = value;} }

    public UnityEvent onCorrectPassage = new UnityEvent();
    public UnityEvent onIncorrectPassage = new UnityEvent();

    private bool _isGrounded;
    private Rigidbody2D _rigidbody;
    private Figure _figure;
    public Figure Figure => _figure;

    #endregion

    #region Main Methods

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _figure = GetComponent<Figure>();
    }

    private void Update()
    {
        if (_move)
            transform.position = transform.position + transform.right * _speed * Time.deltaTime;
    }

    private void FixedUpdate()
    {
        CheckGround();
    }

    #endregion

    #region Helper Methods

    public void CorrectPassage()
    {
        IncreaseSpeed();
        onCorrectPassage?.Invoke();
    }

    public void IncorrectPassage()
    {
        StopMove();
        onIncorrectPassage?.Invoke();
    }

    public void Jump()
    {   
        if (_isGrounded)
            _rigidbody.AddForce(transform.up * _jumpForce, ForceMode2D.Impulse);
    }

    private void IncreaseSpeed()
    {
        if (_speed + _increaseSpeed <= _maxSpeed)
            _speed += _increaseSpeed;
    }

    private void CheckGround()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.8f);
        _isGrounded = colliders.Length > 1;
    }

    private void StopMove()
    {
        _move = false;
    }

    #endregion
}
