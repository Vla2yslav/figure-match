﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Preferences", menuName = "FM/Preferences")]
public class Preferences : ScriptableObject
{
    public List<Gate_Template> Gates = new List<Gate_Template>();
    public List<Figure_Template> Figures = new List<Figure_Template>();
    public List<Color> Colors = new List<Color>();
    public Color DeactivateColor;
    
}
