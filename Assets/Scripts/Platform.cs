﻿using UnityEngine;
using UnityEngine.Events;

public class Platform : MonoBehaviour
{
    #region Fields

    public UnityEvent onChangePlatform = new UnityEvent();
    private Gate[] _gates;
    
    #endregion

    #region Main Methods

    private void Start()
    {
        _gates = new Gate[transform.childCount - 1];
        _gates = GetComponentsInChildren<Gate>();

        InitializeGates();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        onChangePlatform?.Invoke();
    }

    #endregion

    #region Helper Methods

    public void InitializeGates()
    {
        foreach(Gate gate in _gates)
            gate.InitializeGate();
    }

    #endregion
}
