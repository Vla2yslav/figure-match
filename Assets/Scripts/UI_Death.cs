﻿using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class UI_Death : MonoBehaviour
{
    #region Fields

    [Header("Main settings")]
    [SerializeField] private TextMeshProUGUI _scoreText;
    [SerializeField] private TextMeshProUGUI _recordText;

    #endregion

    #region Main Methods

    private void OnEnable()
    {
        if (PlayerPrefs.HasKey("score"))
            _scoreText.text = PlayerPrefs.GetInt("score").ToString();
        
        if (PlayerPrefs.HasKey("record"))
            _recordText.text = PlayerPrefs.GetInt("record").ToString();
    }

    #endregion

    #region Helper Methods

    public void ClickHomeButton()
    {
        SceneManager.LoadScene("Main");
    }

    public void ClickRestartButton()
    {
        SceneManager.LoadScene("Restart");
    }

    #endregion
}
