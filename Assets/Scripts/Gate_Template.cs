﻿using UnityEngine;

[CreateAssetMenu(fileName = "Gate", menuName = "FM/Gate_Data")]
public class Gate_Template : ScriptableObject
{
   public string Name;
   public Sprite[] Sprites;
}
