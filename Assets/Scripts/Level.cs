﻿using UnityEngine;

public class Level : MonoBehaviour
{
    #region Fields

    [Header("Main settings")]
    [SerializeField] [Range(0, 5)] private float _heightLevelDistance;
    private GameObject[] _platforms;
    private int _indexLastPlatform, _indexFirstPlatform;
    private float _widthPlatform;

    #endregion

    #region Main Methods

    private void Awake()
    {
        _platforms = new GameObject[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
        {
            _platforms[i] = transform.GetChild(i).gameObject;
            if (i > 0)
                _widthPlatform = _platforms[i].transform.position.x - _platforms[i - 1].transform.position.x;
        }   

        foreach(GameObject plat in _platforms)
            plat.GetComponent<Platform>().onChangePlatform.AddListener(() => ChangePlatform(plat.name));
        
        _indexFirstPlatform = _platforms.Length - 1;
    }

    #endregion

    #region Helper Methods

    private void ChangePlatform(string index)
    {
        if (int.Parse(index) != _indexLastPlatform)
        {
            _platforms[_indexLastPlatform].transform.position = GenerateCoordinate();
            _platforms[_indexLastPlatform].GetComponent<Platform>().InitializeGates();
            _indexFirstPlatform = _indexLastPlatform;
            _indexLastPlatform++;

            if (_indexLastPlatform >= _platforms.Length)
                _indexLastPlatform = 0;
        }
    }

    private Vector3 GenerateCoordinate()
    {
        Vector3 position = new Vector3(_platforms[_indexFirstPlatform].transform.position.x + _widthPlatform, 
            _platforms[_indexFirstPlatform].transform.position.y + RandomizeHeightLevelPlatform(), 0);

        return position;
    }

    private float RandomizeHeightLevelPlatform()
    {
        /* float distance = 0;
        int randLvl = Random.Range(1, 4);

        if (randLvl == 1)
            distance = _heightLevelDistance;
        else if (randLvl == 2)
            distance = -_heightLevelDistance;
 */

        float distance = Random.Range(-_heightLevelDistance, _heightLevelDistance);

        return distance;
    }
    
    #endregion
}
