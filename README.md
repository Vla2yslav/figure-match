FIGURE MATCH
---

Figure Match is a small game whose goal is to choose the correct figure and color to get through the gate. The more points collected, the faster the player moves, thereby complicating the game.
 
The project was created to demonstrate the purity of the code and the organization of the architecture of the game.

---
Demo
---

![](demo.gif)